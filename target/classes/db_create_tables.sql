-- языки
CREATE TABLE IF NOT EXISTS language (
id SERIAL NOT NULL,
code VARCHAR (10) NOT NULL UNIQUE,
name VARCHAR(10) NOT NULL,
PRIMARY KEY (id));

-- типы автомобиля
CREATE TABLE IF NOT EXISTS auto_type (
id SERIAL NOT NULL,
name VARCHAR(100) NOT NULL UNIQUE,
PRIMARY KEY (id));

-- марка автомобиля
CREATE TABLE IF NOT EXISTS auto_model (
id SERIAL NOT NULL,
name VARCHAR(100) NOT NULL UNIQUE,
PRIMARY KEY (id));

-- автомобиль
CREATE TABLE IF NOT EXISTS auto (
id VARCHAR(15) NOT NULL UNIQUE,
auto_type_id BIGINT UNSIGNED NOT NULL,
auto_model_id BIGINT UNSIGNED NOT NULL,
color VARCHAR(50) DEFAULT NULL,
description TEXT DEFAULT NULL,
PRIMARY KEY (id),
FOREIGN KEY (auto_type_id) REFERENCES auto_type (id),
FOREIGN KEY (auto_model_id) REFERENCES auto_model (id));

-- город
CREATE TABLE IF NOT EXISTS city (
id SERIAL NOT NULL,
name VARCHAR(200) NOT NULL UNIQUE,
PRIMARY KEY (id));

-- пользователь
CREATE TABLE IF NOT EXISTS users (
id SERIAL NOT NULL,
first_name VARCHAR(255) NOT NULL,
last_name VARCHAR(255) NOT NULL,
phone VARCHAR(15) NOT NULL,
email VARCHAR(255) NOT NULL UNIQUE,
password VARCHAR(255) NOT NULL,
city_id BIGINT UNSIGNED NOT NULL,
is_admin BOOL DEFAULT false,
is_driver BOOL DEFAULT false,
is_recipient BOOL DEFAULT false,
is_blocked BOOL DEFAULT false,
PRIMARY KEY (id),
FOREIGN KEY (city_id) REFERENCES city (id));

-- водитель
CREATE TABLE IF NOT EXISTS driver (
id BIGINT(10) UNSIGNED NOT NULL UNIQUE,
user_id BIGINT UNSIGNED NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (user_id) REFERENCES users (id));

-- водитель-автомобиль
CREATE TABLE IF NOT EXISTS driver_auto (
driver_id SERIAL NOT NULL, -- Serial ?
auto_id VARCHAR(10) NOT NULL UNIQUE,
PRIMARY KEY (driver_id, auto_id),
FOREIGN KEY (driver_id) REFERENCES driver (id),
FOREIGN KEY (auto_id) REFERENCES auto (id));

-- тип уязвимости
CREATE TABLE IF NOT EXISTS disability_type (
id SERIAL NOT NULL,
name VARCHAR(20) NOT NULL UNIQUE ,
PRIMARY KEY (id));

-- группа инвалидности
CREATE TABLE IF NOT EXISTS disability_group (
id SERIAL NOT NULL,
name VARCHAR(20) NOT NULL UNIQUE ,
PRIMARY KEY (id));

-- получатель услуги
CREATE TABLE IF NOT EXISTS recipient (
id SERIAL NOT NULL,
user_id BIGINT UNSIGNED NOT NULL,
disability_type_id BIGINT UNSIGNED NOT NULL,
disability_group_id BIGINT UNSIGNED DEFAULT NULL,
address VARCHAR(255) DEFAULT NULL,
PRIMARY KEY (id),
FOREIGN KEY (user_id) REFERENCES users (id),
FOREIGN KEY (disability_type_id) REFERENCES disability_type (id),
FOREIGN KEY (disability_group_id) REFERENCES disability_group (id));

-- статус услуги
CREATE TABLE IF NOT EXISTS service_status (
id SERIAL NOT NULL,
name VARCHAR(50) NOT NULL UNIQUE,
PRIMARY KEY (id));

-- услуга
CREATE TABLE IF NOT EXISTS service (
id SERIAL NOT NULL,
driver_id BIGINT UNSIGNED DEFAULT NULL,
recipient_id BIGINT UNSIGNED NOT NULL,
from_place VARCHAR(255) NOT NULL,
to_place VARCHAR(255) NOT NULL,
date DATE NOT NULL,
time TIME NOT NULL,
with_helper BOOL DEFAULT false,
service_status_id BIGINT UNSIGNED DEFAULT 1,
PRIMARY KEY (id),
FOREIGN KEY (driver_id) REFERENCES driver (id),
FOREIGN KEY (recipient_id) REFERENCES recipient (id),
FOREIGN KEY (service_status_id) REFERENCES service_status (id));

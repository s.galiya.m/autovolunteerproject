<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="signup" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>

<link rel="stylesheet" href="css/style.css">
<main>
    <h3><fmt:message key="signup.success"/> ${login}!</h3>
    <a href="login" class="btn btn-lg btn-primary mt-4 fw-bold bg-gradient"><fmt:message key="login"/></a>
</main>

<jsp:include page="bottom.jsp"/>
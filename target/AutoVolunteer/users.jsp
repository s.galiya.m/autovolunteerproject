<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="users" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>
<main>
    <h3 class="my-4 text-primary"><fmt:message key="users"/></h3>
    <div class="mx-5">
        <table class="table table-striped table-hover">
            <thead class="table-primary">
                <tr>
                    <th scope="col"><fmt:message key="user.id"/></th>
                    <th scope="col"><fmt:message key="firstname"/></th>
                    <th scope="col"><fmt:message key="lastname"/></th>
                    <th scope="col"><fmt:message key="order.phone"/></th>
                    <th scope="col"><fmt:message key="order.email"/></th>
                    <th scope="col"><fmt:message key="city"/></th>
                    <th scope="col"><fmt:message key="driver"/></th>
                    <th scope="col"><fmt:message key="order.recipient"/></th>
                    <th scope="col"><fmt:message key="user.status"/></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="user" begin="1" items="${all_users}">
                <tr class="border-bottom">
                    <td>${user.id}</td>
                    <td>${user.firstName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.phone}</td>
                    <td>${user.email}</td>
                    <td>${user.cityId}</td>
                    <td>
                        <c:choose>
                            <c:when test="${user.driver}">
                                <fmt:message key="yes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="no"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${user.recipient}">
                                <fmt:message key="yes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="no"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${user.blocked}">
                                <fmt:message key="yes"/>
                            </c:when>
                            <c:otherwise>
                                <fmt:message key="no"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <form action="/user_block" method="post" class="m-0" style="text-align: right" >
                            <input type="hidden" name="user_status" value="${user.blocked}"/>
                            <input type="hidden" name="user_id" value="${user.id}"/>
                            <input type="submit" style="min-width: 140px;"
                            <c:choose>
                                <c:when test="${user.blocked}">
                                    value="<fmt:message key="unblock"/>" class="btn btn-sm btn-success"
                                </c:when>
                                <c:otherwise>
                                    value="<fmt:message key="block"/>" class="btn btn-sm btn-danger"
                                </c:otherwise>
                            </c:choose>
                            />
                        </form>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</main>
<jsp:include page="bottom.jsp"/>
-- языки
INSERT INTO language (code, name) VALUES ('en_US', 'Eng');
INSERT INTO language (code, name) VALUES ('ru_RU', 'Рус');

-- типы автомобиля
INSERT INTO auto_type (name) VALUES ('седан');
INSERT INTO auto_type (name) VALUES ('универсал');
INSERT INTO auto_type (name) VALUES ('купе');
INSERT INTO auto_type (name) VALUES ('хэчбэк');
INSERT INTO auto_type (name) VALUES ('микроавтобус');
INSERT INTO auto_type (name) VALUES ('внедорожник');
INSERT INTO auto_type (name) VALUES ('кроссовер');
INSERT INTO auto_type (name) VALUES ('пикап');
INSERT INTO auto_type (name) VALUES ('минивэн');
INSERT INTO auto_type (name) VALUES ('фургон');

-- марка автомобиля
INSERT INTO auto_model (name) VALUES ('Toyota');
INSERT INTO auto_model (name) VALUES ('LADA');
INSERT INTO auto_model (name) VALUES ('Honda');
INSERT INTO auto_model (name) VALUES ('Audi');
INSERT INTO auto_model (name) VALUES ('Hyundai');
INSERT INTO auto_model (name) VALUES ('Skoda');
INSERT INTO auto_model (name) VALUES ('ГАЗ');
INSERT INTO auto_model (name) VALUES ('Mercedes');

-- автомобиль
INSERT INTO auto (id, auto_type_id, auto_model_id, color) VALUES ('KZ777EOG16', 2, 2, 'красный');
INSERT INTO auto (id, auto_type_id, auto_model_id, color) VALUES ('KZ962EGB02', 1, 2, 'черный');
INSERT INTO auto (id, auto_type_id, auto_model_id, color) VALUES ('KZ555KRI16', 1, 8, 'белый');

-- город
INSERT INTO city (name) VALUES ('Усть-Каменогорск');
INSERT INTO city (name) VALUES ('Алматы');
INSERT INTO city (name) VALUES ('Караганда');

-- пользователь
INSERT INTO users (first_name, last_name, phone, email, password, city_id, is_admin) VALUES ('Админ', 'Админов', '+770012345678', 'admin@test.com', '21232F297A57A5A743894A0E4A801FC3', 1, true); -- admin
INSERT INTO users (first_name, last_name, phone, email, password, city_id, is_driver) VALUES ('Водитель1', 'Фам1', '+770012345678', 'test1@test.com', '24C9E15E52AFC47C225B757E7BEE1F9D', 1, true); -- user1
INSERT INTO users (first_name, last_name, phone, email, password, city_id, is_recipient) VALUES ('Получатель2', 'Фам2', '+770012345678', 'test2@test.com', '7E58D63B60197CEB55A1C487989A3720', 1, true); -- user2
INSERT INTO users (first_name, last_name, phone, email, password, city_id, is_driver) VALUES ('Водитель3', 'Фам3', '+770012345678', 'test3@test.com', '92877AF70A45FD6A2ED7FE81E1236B78', 2, true); -- user3
INSERT INTO users (first_name, last_name, phone, email, password, city_id, is_recipient) VALUES ('Получатель4', 'Фам4', '+770012345678', 'test4@test.com', '3F02EBE3D7929B091E3D8CCFDE2F3BC6', 2, true); -- user4
INSERT INTO users (first_name, last_name, phone, email, password, city_id, is_driver) VALUES ('Водитель5', 'Фам5', '+770012345678', 'test5@test.com', '0a791842f52a0acfbb3a783378c066b8', 3, true); -- user5
INSERT INTO users (first_name, last_name, phone, email, password, city_id, is_recipient) VALUES ('Получатель6', 'Фам6', '+770012345678', 'test6@test.com', 'affec3b64cf90492377a8114c86fc093', 3, true); -- user6

-- водитель
INSERT INTO driver (id, user_id) VALUES ('123456', 2);
INSERT INTO driver (id, user_id) VALUES ('654321', 4);
INSERT INTO driver (id, user_id) VALUES ('258963', 6);

-- водитель-автомобиль
INSERT INTO driver_auto (driver_id, auto_id) VALUES ('123456', 'KZ777EOG16');
INSERT INTO driver_auto (driver_id, auto_id) VALUES ('654321', 'KZ962EGB02');
INSERT INTO driver_auto (driver_id, auto_id) VALUES ('258963', 'KZ555KRI16');

-- тип уязвимости
INSERT INTO disability_type (name) VALUES ('по зрению');
INSERT INTO disability_type (name) VALUES ('по слуху');
INSERT INTO disability_type (name) VALUES ('инвалидное кресло');
INSERT INTO disability_type (name) VALUES ('пенсионер');

-- группа инвалидности

INSERT INTO disability_group (name) VALUES ('первая');
INSERT INTO disability_group (name) VALUES ('вторая');
INSERT INTO disability_group (name) VALUES ('третья');

-- получатель услуги
INSERT INTO recipient (user_id, disability_type_id, disability_group_id) VALUES (3, 1, 1);
INSERT INTO recipient (user_id, disability_type_id, disability_group_id) VALUES (5, 3, 1);
INSERT INTO recipient (user_id, disability_type_id, disability_group_id) VALUES (7, 2, 2);


-- статус услуги
INSERT INTO service_status (name) VALUES ('создана');
INSERT INTO service_status (name) VALUES ('в работе');
INSERT INTO service_status (name) VALUES ('выполнена');

-- услуга
INSERT INTO service (recipient_id, from_place, to_place, date, time) VALUES (1, 'Адрес пункта отправки', 'Адрес пункта назначения', '2021-01-20', '15:30:00');
INSERT INTO service (recipient_id, from_place, to_place, date, time, with_helper) VALUES (2, 'Адрес пункта отправки', 'Адрес пункта назначения', '2021-02-01', '12:00:00', true);
INSERT INTO service (recipient_id, from_place, to_place, date, time, with_helper) VALUES (3, 'Адрес пункта отправки', 'Адрес пункта назначения', '2021-03-02', '16:00:00', true);


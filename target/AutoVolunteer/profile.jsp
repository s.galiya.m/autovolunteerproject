<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="profile" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>
<main>
    <h3 class="my-4 text-primary"><fmt:message key="profile"/></h3>
    <div class="d-flex mx-5 justify-content-around flex-wrap">
        <div class="prof-card flex-fill d-flex flex-column align-items-start p-4 border rounded mx-2">
            <p><fmt:message key="email"/>: ${user.email}</p>
            <p><fmt:message key="user.id"/>: ${user.id}</p>
            <p><fmt:message key="firstname"/>: ${user.firstName}</p>
            <p><fmt:message key="lastname"/>: ${user.lastName}</p>
            <p><fmt:message key="order.phone"/>: ${user.phone}</p>
            <p><fmt:message key="city"/>: ${sessionScope.city_current[0].name}</p>

            <form action="/profile_edit">
                <input type="submit" class="btn btn-sm btn-primary" value="<fmt:message key="order.edit"/>">
            </form>
        </div>

        <c:choose>
            <c:when test="${sessionScope.is_driver}">
                <div class="prof-card flex-fill d-flex flex-column align-items-start p-4 border rounded mx-2">
                    <p class="mb-4"><fmt:message key="order.driver.id"/>: ${driver.id}</p>
                    <p><fmt:message key="order.auto.id"/>: ${auto.id}</p>
                    <p><fmt:message key="order.auto.color"/>: ${auto.color}</p>
                    <p><fmt:message key="order.auto.type"/>: ${auto.autoType}</p>
                    <p><fmt:message key="order.auto.model"/>: ${auto.autoModel}</p>
                    <p><fmt:message key="signup.auto.desc"/>: ${auto.description}</p>

                    <form action="/profile_auto_edit">
                        <input type="hidden" name="auto_id" value="${auto.id}">
                        <input type="submit" class="btn btn-sm btn-primary" value="<fmt:message key="order.edit"/>">
                    </form>
                </div>
            </c:when>
            <c:when test="${sessionScope.is_recipient}">
                <div class="prof-card flex-fill d-flex flex-column align-items-start p-4 border rounded mx-2">
                    <p><fmt:message key="order.recipient.id"/>: ${recipient.id}</p>
                    <p><fmt:message key="order.dis.type"/>:
                        <c:forEach var="type" items="${dis_types}">
                            <c:if test="${type.id == recipient.disabilityTypeId}">
                                ${type.name}
                            </c:if>
                        </c:forEach>
                    </p>
                    <p><fmt:message key="order.dis.gr"/>:
                        <c:forEach var="gr" items="${dis_groups}">
                            <c:if test="${gr.id == recipient.disabilityGroupId}">
                                ${gr.name}
                            </c:if>
                        </c:forEach>
                    </p>
                    <p><fmt:message key="address"/>: ${recipient.address}</p>

                    <form action="/profile_recipient_edit">
                        <input type="hidden" name="recipient_id" value="${recipient.id}">
                        <input type="submit" class="btn btn-sm btn-primary" value="<fmt:message key="order.edit"/>">
                    </form>
                </div>
            </c:when>
        </c:choose>
    </div>
</main>
<jsp:include page="bottom.jsp"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="order.take" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>


<c:choose>
    <c:when test="${sessionScope.is_driver}">
<%--        <c:url value="order_take_success" var="order_taken">--%>
<%--            <c:param name="order_id" value="${order_id}"/>--%>
<%--        </c:url>--%>
<%--        <c:redirect url="${order_taken}"/>--%>

    </c:when>
    <c:otherwise>
        <main>
            <form action="order_take_success" method="post" class="d-flex flex-column align-items-center">
                <h3 class="my-4 ms-5 text-primary"><fmt:message key="order.take.title1"/>${order_id} <fmt:message key="order.take.title2"/> ${cities[order_city_id - 1].name}</h3>
                <div class="form-content ms-5">
                    <input type="hidden" name="order_id" value="${order_id}"/>
                    <label for="selectDriver" class="d-flex"><span class="text-danger">*</span><span class="label-title"><fmt:message key="order.driver"/></span>
                        <select id="selectDriver" name="driver" class="form-control ms-2" required>
                            <option value=""><fmt:message key="select.driver"/></option>
                            <c:forEach var="driver" items="${drivers}" >
                                <c:if test="${!users[driver.userId-1].blocked}">
                                    <option value="${driver.id}">${users[driver.userId-1].firstName} ${users[driver.userId-1].lastName}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </label>
                </div>
                <div class="d-flex">
                    <a href="main" class="btn btn-lg btn-secondary text-light mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="cancel"/></a>
                    <button type="submit" class="btn btn-lg btn-success mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="order.assign"/></button>
                </div>
            </form>
        </main>
    </c:otherwise>
</c:choose>
<jsp:include page="bottom.jsp"/>
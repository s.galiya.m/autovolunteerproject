<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="order.edit" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>

<%--<jsp:useBean id="now" class="java.util.Date"/>--%>

<main>
    <form action="order_edit_success" method="post" class="d-flex flex-column align-items-center">
        <h3 class="my-4 ms-5 text-primary"><fmt:message key="order.edit.title"/> ${order_id}</h3>
        <div class="form-content ms-5">
            <input type="hidden" name="order_id" value="${order_id}"/>
            <c:choose>
                <c:when test="${sessionScope.is_admin}">
                    <label for="selectRecipient" class="d-flex pb-3"><span class="text-danger">*</span><span class="label-title"><fmt:message key="order.recipient"/></span>
                        <select id="selectRecipient" name="recipient" class="form-control ms-2" required>
                            <option value=""><fmt:message key="select.recipient"/></option>
                            <c:forEach var="recipient" items="${recipients}" >
                                <c:if test="${!users[recipient.userId-1].blocked}">
                                    <option value="${recipient.id}" <c:if test="${order_recipient_id == recipient.id}">selected</c:if>>
                                        ${users[recipient.userId-1].firstName} ${users[recipient.userId-1].lastName}
                                    </option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </label>
                </c:when>
                <c:otherwise>
                    <input type="hidden" id="inputRecipient" name="recipient" value="${order_recipient_id}"/>
                </c:otherwise>
            </c:choose>
            <label for="inputFrom" class="d-flex pb-3"> <span class="text-danger">*</span><span class="label-title"><fmt:message key="order.from"/></span>
                <input type="text" id="inputFrom" name="from" class="form-control ms-2" maxlength="255" value="${order_from}" required/></label>

            <label for="inputTo" class="d-flex pb-3"> <span class="text-danger">*</span><span class="label-title"><fmt:message key="order.to"/></span>
                <input type="text" id="inputTo" name="to" class="form-control ms-2" maxlength="255" value="${order_to}" required/></label>

            <label for="inputDate" class="d-flex pb-3"> <span class="text-danger">*</span><span class="label-title"><fmt:message key="order.date"/></span>
                <input type="date" id="inputDate" name="date" min="${current_date}" class="form-control ms-2" value="${order_date}" pattern="" required/></label>

            <label for="inputTime" class="d-flex pb-3"> <span class="text-danger">*</span><span class="label-title"><fmt:message key="order.time"/></span>
                <input type="time" id="inputTime" name="time" class="form-control ms-2" value="${order_time}" pattern="" required/></label>

            <label for="inputHelper" class="d-flex align-items-center"> <span class="text-danger">*</span><span class="label-title"><fmt:message key="order.helper"/></span>
                <input type="checkbox" id="inputHelper" name="helper" class="ms-2" value="true" <c:if test="${order_helper}">checked</c:if> /></label>
        </div>
        <div class="d-flex">
            <a href="main" class="btn btn-lg btn-secondary text-light mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="cancel"/></a>
            <button type="submit" class="btn btn-lg btn-success mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="update"/></button>
        </div>
    </form>
</main>
<jsp:include page="bottom.jsp"/>
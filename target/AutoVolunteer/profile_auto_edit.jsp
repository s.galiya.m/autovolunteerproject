<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="auto.edit" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>
<main>
    <form action="profile_auto_edit_success" method="post" class="d-flex flex-column align-items-center">
        <h3 class="my-4 ms-5 text-primary"><fmt:message key="auto.edit"/> ${auto.id}</h3>
        <div class="form-content ms-5">
            <input type="hidden" name="auto_id" value="${auto.id}">
            <label for="inputColor" class="d-flex pb-3"> <span class="label-title"><fmt:message key="order.auto.color"/></span>
                <input type="text" id="inputColor" name="color" class="form-control ms-3" maxlength="50" value="${auto.color}" autofocus/></label>

            <label for="selectAutoType" class="d-flex pb-3"><span class="text-danger">*</span><span class="label-title"><fmt:message key="order.auto.type"/></span>
                <select id="selectAutoType" name="auto_type" class="form-control ms-2" required>
                    <option value=""><fmt:message key="signup.auto.type"/></option>
                    <c:forEach var="auto_type" items="${auto_types}" >
                        <option value="${auto_type.id}" <c:if test="${auto.autoTypeId == auto_type.id}">selected</c:if>>
                            ${auto_type.name}
                        </option>
                    </c:forEach>
                </select>
            </label>

            <label for="selectAutoModel" class="d-flex pb-3"><span class="text-danger">*</span><span class="label-title"><fmt:message key="order.auto.model"/></span>
                <select id="selectAutoModel" name="auto_model" class="form-control ms-2" required>
                    <option value=""><fmt:message key="signup.auto.model"/></option>
                    <c:forEach var="auto_model" items="${auto_models}" >
                        <option value="${auto_model.id}" <c:if test="${auto.autoModelId == auto_model.id}">selected</c:if>>
                            ${auto_model.name}
                        </option>
                    </c:forEach>
                </select>
            </label>

            <label for="textDesc" class="d-flex pb-3"><span class="label-title"><fmt:message key="signup.auto.desc"/></span>
                <textarea  id="textDesc" name="auto_desc" class="form-control ms-3" maxlength="200">${auto.description}</textarea></label>
        </div>
        <div class="d-flex">
            <a href="profile" class="btn btn-lg btn-secondary text-light mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="cancel"/></a>
            <button type="submit" class="btn btn-lg btn-success mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="update"/></button>
        </div>
    </form>
</main>
<jsp:include page="bottom.jsp"/>
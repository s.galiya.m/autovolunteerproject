<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="signup" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>

<main class="form-sign-up">
    <p class="mb-3 text-danger">
        <c:choose>
            <c:when test="${sessionScope.signup_err == 'email_exist'}">
                <fmt:message key="signup.err.email"/>
            </c:when>
            <c:when test="${sessionScope.signup_err == 'email_not_valid'}">
                <fmt:message key="signup.err.email.valid"/>
            </c:when>
            <c:when test="${sessionScope.signup_err == 'auto_exist'}">
                <fmt:message key="signup.err.auto"/>
            </c:when>
            <c:when test="${sessionScope.signup_err == 'driver_exist'}">
                <fmt:message key="signup.err.driver"/>
            </c:when>
            <c:when test="${sessionScope.signup_err == 'auto_and_driver_exist'}">
                <fmt:message key="signup.err.auto.driver"/>
            </c:when>
        </c:choose>
    </p>
    <c:remove var="signup_err" scope="session"/>

    <form action="signup_success" method="post">
        <h1 class="h3 mb-3 fw-normal"><fmt:message key="signup"/></h1>
        <div class="d-flex justify-content-center mx-n3">
            <div class="form-item mx-3">
                <label for="inputName" class="d-flex text-danger"> *
                <input type="text" id="inputName" name="name" class="form-control ms-2" maxlength="255" placeholder="<fmt:message key="firstname"/>" required autofocus/></label>

                <label for="inputLastName" class="d-flex text-danger"> *
                <input type="text" id="inputLastName" name="lastname" maxlength="255" class="form-control ms-2" placeholder="<fmt:message key="lastname"/>" required/></label>

                <label for="inputPhone" class="d-flex text-danger"> *
                <input type="tel" id="inputPhone" name="phone" class="form-control ms-2" minlength="12" maxlength="12" pattern="(\+?\d[- .]*){11}" placeholder="+77001234567" required/></label>

                <label for="inputEmail" class="d-flex text-danger"> *
                <input type="email" id="inputEmail" name="email" class="form-control ms-2" maxlength="255" placeholder="example@example.com" pattern="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$" required/></label>

                <label for="inputPassword" class="d-flex text-danger"> *
                <input type="password" id="inputPassword" name="password" class="form-control ms-2" minlength="4"
                       maxlength="8" title="Must contain at least 4 and max 8 characters" placeholder="<fmt:message key="pwd"/>" required/></label>

                <label for="inputPasswordConfirm" class="d-flex text-danger"> *
                <input type="password" id="inputPasswordConfirm" name="password-confirm" class="form-control ms-2" placeholder="<fmt:message key="pwd.confirm"/>" minlength="4"
                       maxlength="8" required/></label>

                <label for="selectCity" class="d-flex text-danger"> *
                <select id="selectCity" name="city" class="form-control ms-2" required>
                    <option value=""><fmt:message key="signup.city"/></option>
                    <c:forEach var="city" items="${cities}" >
                        <option value="${city.id}">${city.name}</option>
                    </c:forEach>
                </select></label>

                <label for="selectCity" class="d-flex text-danger"> *
                <select id="selectUserType" name="user_type" class="form-control ms-2" required onchange="setRequiredExtraFields()">
                    <option value=""><fmt:message key="signup.account"/></option>
                    <option value="driver"><fmt:message key="driver"/></option>
                    <option value="recipient"><fmt:message key="service.user"/></option>
                </select></label>
            </div>
            <div class="form-item mx-3" id="driverAccount" style="display: none;">
                <label for="inputDriverID" class="d-flex text-danger"> *
                <input type="text" id="inputDriverID" name="driver_id" class="form-control ms-2" maxlength="6" minlength="6" pattern="[0-9]{6}" placeholder="<fmt:message key="order.driver.id"/>"/></label>

                <label for="inputAutoID" class="d-flex text-danger"> *
                <input type="text" id="inputAutoID" name="auto_id" class="form-control ms-2" maxlength="10" placeholder="<fmt:message key="order.auto.id"/>"/></label>

                <label for="inputAutoColor" class="d-flex">
                <input type="text" id="inputAutoColor" name="auto_color" class="form-control ms-3" style="margin-right: 0;" maxlength="50" placeholder="<fmt:message key="order.auto.color"/>"/></label>

                <label for="selectAutoType" class="d-flex text-danger"> *
                <select id="selectAutoType" name="auto_type" class="form-control ms-2">
                    <option value=""><fmt:message key="signup.auto.type"/></option>
                    <c:forEach var="auto_type" items="${auto_types}" >
                        <option value="${auto_type.id}">${auto_type.name}</option>
                    </c:forEach>
                </select></label>

                <label for="selectAutoModel" class="d-flex text-danger"> *
                <select id="selectAutoModel" name="auto_model" class="form-control ms-2">
                    <option value=""><fmt:message key="signup.auto.model"/></option>
                    <c:forEach var="auto_model" items="${auto_models}" >
                        <option value="${auto_model.id}">${auto_model.name}</option>
                    </c:forEach>
                </select></label>

                <label for="textAutoDesc" class="d-flex">
                <textarea  id="textAutoDesc" name="auto_desc" class="form-control ms-3" maxlength="200" placeholder="<fmt:message key="signup.auto.desc"/>"></textarea></label>
            </div>
            <div class="form-item mx-3" id="recipientAccount" style="display: none;">
                <label for="selectCity" class="d-flex text-danger"> *
                <select id="selectDisType" name="dis_type" class="form-control ms-2">
                    <option value=""><fmt:message key="signup.dis.type"/></option>
                    <c:forEach var="dis_type" items="${dis_types}" >
                        <option value="${dis_type.id}">${dis_type.name}</option>
                    </c:forEach>
                </select></label>

                <label for="selectCity" class="d-flex">
                <select id="selectDisGroup" name="dis_gr" class="form-control ms-3" style="margin-right: 0;">
                    <option value=""><fmt:message key="signup.dis.gr"/></option>
                    <c:forEach var="dis_group" items="${dis_groups}" >
                        <option value="${dis_group.id}">
                                ${dis_group.name}
                        </option>
                    </c:forEach>
                </select></label>

                <label for="textAddress" class="d-flex">
                <textarea  id="textAddress" name="address" class="form-control ms-3" style="margin-right: 0;" maxlength="255" placeholder="<fmt:message key="address"/>"></textarea></label>
            </div>
        </div>
        <button type="submit" class="btn btn-lg btn-success mt-3 ms-3 bg-gradient"><fmt:message key="signup"/></button>
    </form>
</main>
<jsp:include page="bottom.jsp"/>

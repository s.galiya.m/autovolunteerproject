CREATE OR REPLACE VIEW services_by_status_and_city AS
SELECT s.*,
       u.first_name, u.last_name, u.phone, u.email, u.city_id,
       r.user_id, r.disability_type_id, r.disability_group_id,
       d.user_id as driver_user_id, ud.first_name as driver_first_name, ud.last_name as driver_last_name, ud.phone as driver_phone,
       da.auto_id,
       a.color,
       at.name as auto_type,
       am.name as auto_model
FROM service AS s
         INNER JOIN recipient AS r ON s.recipient_id = r.id
         LEFT JOIN driver as d ON s.driver_id = d.id
         INNER JOIN users AS u ON r.user_id = u.id
         LEFT JOIN users AS ud ON d.user_id = ud.id
         LEFT JOIN driver_auto as da ON d.id = da.driver_id
         LEFT JOIN auto as a ON da.auto_id = a.id
         LEFT JOIN auto_type as at ON a.auto_type_id = at.id
         LEFT JOIN auto_model as am ON a.auto_model_id = am.id;

CREATE OR REPLACE VIEW auto_extended AS
SELECT a.*, da.driver_id, d.user_id, at.name as type_name, am.name as model_name
FROM auto AS a
    INNER JOIN driver_auto AS da ON a.id = da.auto_id
    INNER JOIN driver AS d ON da.driver_id = d.id
    LEFT JOIN auto_type AS at ON a.auto_type_id = at.id
    LEFT JOIN auto_model AS am ON a.auto_model_id = am.id;
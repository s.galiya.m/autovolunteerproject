<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="order.edit" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>

<main>
    <h2 class="my-4 text-primary">
        <c:choose>
            <c:when test="${sessionScope.is_admin}"><fmt:message key="order.edit.msg.admin1"/>${order_id} <fmt:message key="order.edit.msg.admin2"/> ${order_owner} <fmt:message key="order.edit.msg.end"/></c:when>
            <c:otherwise><fmt:message key="order.edit.msg.st"/>${order_id} <fmt:message key="order.edit.msg.end"/></c:otherwise>
        </c:choose>
    </h2>
    <a href="main" class="btn btn-md btn-primary fw-bold bg-gradient"><fmt:message key="to.main"/></a>
</main>

<jsp:include page="bottom.jsp"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="recipient.edit" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>
<main>
    <form action="profile_recipient_edit_success" method="post" class="d-flex flex-column align-items-center">
        <h3 class="my-4 ms-5 text-primary"><fmt:message key="order.recipient"/> ${recipient.id}</h3>
        <div class="form-content ms-5">
            <input type="hidden" name="recipient_id" value="${recipient.id}">

            <label for="selectDisType" class="d-flex pb-3"><span class="text-danger">*</span><span class="label-title"><fmt:message key="order.dis.type"/></span>
                <select id="selectDisType" name="dis_type" class="form-control ms-2" required>
                    <option value=""><fmt:message key="signup.dis.type"/></option>
                    <c:forEach var="dis_type" items="${dis_types}" >
                        <option value="${dis_type.id}" <c:if test="${recipient.disabilityTypeId == dis_type.id}">selected</c:if>>
                                ${dis_type.name}
                        </option>
                    </c:forEach>
                </select>
            </label>

            <label for="selectDisGr" class="d-flex pb-3"><span class="label-title"><fmt:message key="order.dis.gr"/></span>
                <select id="selectDisGr" name="dis_gr" class="form-control ms-3">
                    <option value=""><fmt:message key="signup.dis.gr"/></option>
                    <c:forEach var="dis_gr" items="${dis_groups}" >
                        <option value="${dis_gr.id}" <c:if test="${recipient.disabilityGroupId == dis_gr.id}">selected</c:if>>
                                ${dis_gr.name}
                        </option>
                    </c:forEach>
                </select>
            </label>

            <label for="textAddress" class="d-flex pb-3"><span class="label-title"><fmt:message key="address"/></span>
                <textarea  id="textAddress" name="address" class="form-control ms-3" maxlength="255">${recipient.address}</textarea></label>
        </div>
        <div class="d-flex">
            <a href="profile" class="btn btn-lg btn-secondary text-light mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="cancel"/></a>
            <button type="submit" class="btn btn-lg btn-success mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="update"/></button>
        </div>
    </form>
</main>
<jsp:include page="bottom.jsp"/>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="main" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>
<main class="my-auto mx-3">
    <h3 class="my-4 text-primary"><fmt:message key="orders"/></h3>
    <c:if test="${!sessionScope.is_admin}">
        <p class="ms-3 text-success fs-5" style="text-align: left"><fmt:message key="welcome"/> ${user_name}!</p>
    </c:if>

    <c:if test="${sessionScope.is_admin || sessionScope.is_recipient}">
        <a href="order_add" class="btn btn-md btn-primary fw-bold bg-gradient ms-3 mb-4 d-block " style="width: fit-content"><fmt:message key="order.add"/></a>
    </c:if>

    <c:choose>
        <c:when test="${sessionScope.is_admin}">
            <c:set var="cities_temp" value="${cities}"/>
        </c:when>
        <c:otherwise>
            <c:set var="cities_temp" value="${sessionScope.city_current}"/>
        </c:otherwise>
    </c:choose>

    <c:forEach var="city" items="${cities_temp}">
    <div class="mb-5">
        <div class="d-flex">
            <p class="text-primary fs-5 ms-3 fw-bold">${city.name}</p>
        </div>
        <div class="d-flex justify-content-center">
            <div class="service-col flex-fill mx-2">
                <p class="text-secondary fs-4 fw-bold"><fmt:message key="orders.new"/></p>
                <c:forEach var="service" items="${sessionScope.services_created[city.id - 1]}" >
                    <c:if test="${(sessionScope.user_id == service.userId) || sessionScope.is_admin || sessionScope.is_driver}">
                        <c:set var="this_datetime" value="${service.date} ${service.time}"/>
                        <div class="order-card text-left p-2 border rounded border-secondary mb-3
                        <c:if test="${this_datetime <= current_datetime}"> expired</c:if>" style="text-align: left">
                            <div class="d-flex flex-wrap">
                                <div class="flex-fill px-1">
                                    <p class="text-primary fs-5 fw-bold"><fmt:message key="order.id"/> ${service.id}:</p>
                                    <p class="fw-bold"><fmt:message key="order.recipient"/>: ${service.firstName}  ${service.lastName}</p>
                                    <p><fmt:message key="order.phone"/>: ${service.phone}</p>
                                    <p><fmt:message key="order.email"/>: ${service.email}</p>
                                    <p><fmt:message key="order.dis.type"/>: ${dis_types[service.disabilityTypeId - 1].name}</p>
                                    <c:if test="${service.disabilityGroupId != null}">
                                        <p><fmt:message key="order.dis.gr"/>: ${dis_groups[service.disabilityGroupId - 1].name}</p>
                                    </c:if>
                                </div>
                                <div class="flex-fill  px-1 mt-5">
                                    <p><fmt:message key="order.from"/>: ${service.fromPlace}</p>
                                    <p><fmt:message key="order.to"/>: ${service.toPlace}</p>
                                    <p><fmt:message key="order.date"/>:
                                        <c:choose>
                                            <c:when test="${sessionScope.locale == 'ru_RU'}">
                                                <fmt:formatDate value="${service.date}" pattern="dd-MM-yyyy"/>
                                            </c:when>
                                            <c:otherwise>${service.date}</c:otherwise>
                                        </c:choose>
                                    </p>
                                    <p><fmt:message key="order.time"/>: <fmt:formatDate value="${service.time}" pattern="HH:mm"/></p>
                                    <c:if test="${service.withHelper}">
                                        <p><fmt:message key="order.helper"/></p>
                                    </c:if>
                                </div>
                            </div>
                            <div class="d-flex flex-wrap">
                                <c:if test="${sessionScope.is_admin || sessionScope.is_recipient}">
                                    <form action="order_edit" method="post">
                                        <input type="hidden" name="order_id" value="${service.id}"/>
                                        <input type="hidden" name="order_recipient_id" value="${service.recipientId}"/>
                                        <input type="hidden" name="order_from" value="${service.fromPlace}"/>
                                        <input type="hidden" name="order_to" value="${service.toPlace}"/>
                                        <input type="hidden" name="order_date" value="${service.date}"/>
                                        <input type="hidden" name="order_time"  value="${timeParts[0]}:${timeParts[1]}"/>
                                        <input type="hidden" name="order_helper" value="${service.withHelper}"/>
                                        <button class="btn btn-sm btn-secondary text-light bg-gradient me-3"><fmt:message key="order.edit"/></button>
                                    </form>
                                    <form action="order_delete" method="post">
                                        <input type="hidden" name="order_id" value="${service.id}"/>
                                        <button class="btn btn-sm btn-danger bg-gradient me-3" onclick="return confirm('<fmt:message key="order.delete.confirm"/>')"><fmt:message key="order.delete"/></button>
                                    </form>
                                </c:if>
                                <c:if test="${sessionScope.is_admin || sessionScope.is_driver}">
                                    <form action="order_take" method="post">
                                        <input type="hidden" name="order_id" value="${service.id}"/>
                                        <input type="hidden" name="order_city_id" value="${city.id}"/>
                                        <button class="btn btn-sm btn-primary bg-gradient
                                            <c:if test="${this_datetime <= current_datetime}"> disabled</c:if>" onclick="return confirm('<fmt:message key="order.take.confirm"/>')">
                                            <fmt:message key="order.take"/></button>
                                    </form>
                                </c:if>
                            </div>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
            <div class="service-col flex-fill mx-2">
                <p class="text-secondary fs-4 fw-bold"><fmt:message key="orders.taken"/></p>
                <c:forEach var="service" items="${sessionScope.services_in_work[city.id - 1]}" >
                    <c:if test="${(sessionScope.user_id == service.userId) || sessionScope.is_admin || sessionScope.user_id == service.driverUserId}">
                        <div class="order-card text-left p-2 border rounded border-success mb-3" style="text-align: left">
                            <div class="d-flex flex-wrap">
                                <div class="flex-fill  px-1">
                                    <p class="text-primary fs-5 fw-bold"><fmt:message key="order.id"/>: ${service.id}</p>
                                    <p class="fw-bold"><fmt:message key="order.recipient"/>: ${service.firstName}  ${service.lastName}</p>
                                    <p><fmt:message key="order.phone"/>: ${service.phone}</p>
                                    <p><fmt:message key="order.email"/>: ${service.email}</p>
                                    <p><fmt:message key="order.dis.type"/>: ${dis_types[service.disabilityTypeId - 1].name}</p>
                                    <c:if test="${service.disabilityGroupId != null}">
                                        <p><fmt:message key="order.dis.gr"/>: ${dis_groups[service.disabilityGroupId - 1].name}</p>
                                    </c:if>
                                </div>
                                <div class="flex-fill  px-1 mt-5">
                                    <p><fmt:message key="order.from"/>: ${service.fromPlace}</p>
                                    <p><fmt:message key="order.to"/>: ${service.toPlace}</p>
                                    <p><fmt:message key="order.date"/>:
                                        <c:choose>
                                            <c:when test="${sessionScope.locale == 'ru_RU'}">
                                                <fmt:formatDate value="${service.date}" pattern="dd-MM-yyyy"/>
                                            </c:when>
                                            <c:otherwise>${service.date}</c:otherwise>
                                        </c:choose>
                                    </p>
                                    <p><fmt:message key="order.time"/>: ${service.time}</p>
                                    <c:if test="${service.withHelper}">
                                        <p><fmt:message key="order.helper"/></p>
                                    </c:if>
                                </div>
                                <div class="flex-fill  px-1 mt-5">
                                    <p class="fw-bold"><fmt:message key="order.driver.id"/>: ${service.driverId}</p>
                                    <p class="fw-bold"><fmt:message key="order.driver"/>: ${service.driverFirstName} ${service.driverLastName}</p>
                                    <p><fmt:message key="order.driver.phone"/>: ${service.driverPhone}</p>
                                    <p class="fw-bold"><fmt:message key="order.auto.id"/>: ${service.autoId}</p>
                                    <p><fmt:message key="order.auto.color"/>: ${service.color}</p>
                                    <p><fmt:message key="order.auto.model"/>: ${service.autoModel}</p>
                                    <p><fmt:message key="order.auto.type"/>: ${service.autoType}</p>
                                </div>
                            </div>
                            <div class="d-flex" style="min-height: 47px">
                                <c:if test="${sessionScope.is_admin || sessionScope.is_recipient}">
                                    <form action="order_done" method="post">
                                        <input type="hidden" name="order_id" value="${service.id}"/>
                                        <button class="btn btn-sm btn-success bg-gradient" onclick="return confirm('<fmt:message key="order.done.confirm"/>')"><fmt:message key="order.done"/></button>
                                    </form>
                                </c:if>
                            </div>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
            <div class="service-col flex-fill mx-2">
                <p class="text-secondary fs-4 fw-bold"><fmt:message key="orders.done"/></p>
                <c:forEach var="service" items="${sessionScope.services_done[city.id - 1]}" >
                    <c:if test="${(sessionScope.user_id == service.userId) || sessionScope.is_admin || sessionScope.user_id == service.driverUserId}">
                        <div class="order-card text-left p-2 border rounded border-primary mb-3" style="text-align: left">
                            <div class="d-flex flex-wrap">
                                <div class="flex-fill  px-1">
                                    <p class="text-primary fs-5 fw-bold"><fmt:message key="order.id"/>: ${service.id}</p>
                                    <p class="fw-bold"><fmt:message key="order.recipient"/>: ${service.firstName}  ${service.lastName}</p>
                                    <p><fmt:message key="order.phone"/>: ${service.phone}</p>
                                    <p><fmt:message key="order.email"/>: ${service.email}</p>
                                    <p><fmt:message key="order.dis.type"/>: ${dis_types[service.disabilityTypeId - 1].name}</p>
                                    <c:if test="${service.disabilityGroupId != null}">
                                        <p><fmt:message key="order.dis.gr"/>: ${dis_groups[service.disabilityGroupId - 1].name}</p>
                                    </c:if>
                                </div>
                                <div class="flex-fill  px-1 mt-5">
                                    <p><fmt:message key="order.from"/>: ${service.fromPlace}</p>
                                    <p><fmt:message key="order.to"/>: ${service.toPlace}</p>
                                    <p><fmt:message key="order.date"/>:
                                        <c:choose>
                                            <c:when test="${sessionScope.locale == 'ru_RU'}">
                                                <fmt:formatDate value="${service.date}" pattern="dd-MM-yyyy"/>
                                            </c:when>
                                            <c:otherwise>${service.date}</c:otherwise>
                                        </c:choose>
                                    </p>
                                    <p><fmt:message key="order.time"/>: ${service.time}</p>
                                    <c:if test="${service.withHelper}">
                                        <p><fmt:message key="order.helper"/></p>
                                    </c:if>
                                </div>
                                <div class="flex-fill  px-1 mt-5">
                                    <p class="fw-bold"><fmt:message key="order.driver.id"/>: ${service.driverId}</p>
                                    <p class="fw-bold"><fmt:message key="order.driver"/>: ${service.driverFirstName} ${service.driverLastName}</p>
                                    <p><fmt:message key="order.driver.phone"/>: ${service.driverPhone}</p>
                                    <p class="fw-bold"><fmt:message key="order.auto.id"/>: ${service.autoId}</p>
                                    <p><fmt:message key="order.auto.color"/>: ${service.color}</p>
                                    <p><fmt:message key="order.auto.model"/>: ${service.autoModel}</p>
                                    <p><fmt:message key="order.auto.type"/>: ${service.autoType}</p>
                                </div>
                            </div>
                            <div class="d-flex" style="min-height: 47px"></div>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
        </div>
    </div>
    </c:forEach>
</main>
<jsp:include page="bottom.jsp"/>
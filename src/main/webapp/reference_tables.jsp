<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="references" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>

<main>
    <h3 class="my-4 text-primary"><fmt:message key="references"/></h3>
    <div class="mx-5 d-flex flex-wrap justify-content-around">
        <div class="m-3">
            <p><fmt:message key="city"/></p>
            <table class="table table-striped table-hover">
                <thead class="table-success">
                <tr>
                    <th scope="col"><fmt:message key="col.id"/></th>
                    <th scope="col"><fmt:message key="col.name"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="city" items="${t_cities}">
                    <tr class="border-bottom">
                        <td>${city.id}</td>
                        <td>${city.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="m-3">
            <p><fmt:message key="locale"/></p>
            <table class="table table-striped table-hover">
                <thead class="table-success">
                <tr>
                    <th scope="col"><fmt:message key="col.id"/></th>
                    <th scope="col"><fmt:message key="col.name"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${t_languages}">
                    <tr class="border-bottom">
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="m-3">
            <p><fmt:message key="order.dis.type"/></p>
            <table class="table table-striped table-hover">
                <thead class="table-success">
                <tr>
                    <th scope="col"><fmt:message key="col.id"/></th>
                    <th scope="col"><fmt:message key="col.name"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${t_dis_types}">
                    <tr class="border-bottom">
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="m-3">
            <p><fmt:message key="order.dis.gr"/></p>
            <table class="table table-striped table-hover">
                <thead class="table-success">
                <tr>
                    <th scope="col"><fmt:message key="col.id"/></th>
                    <th scope="col"><fmt:message key="col.name"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${t_dis_groups}">
                    <tr class="border-bottom">
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="m-3">
            <p><fmt:message key="order.auto.type"/></p>
            <table class="table table-striped table-hover">
                <thead class="table-success">
                <tr>
                    <th scope="col"><fmt:message key="col.id"/></th>
                    <th scope="col"><fmt:message key="col.name"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${t_auto_types}">
                    <tr class="border-bottom">
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="m-3">
            <p><fmt:message key="order.auto.model"/></p>
            <table class="table table-striped table-hover">
                <thead class="table-success">
                <tr>
                    <th scope="col"><fmt:message key="col.id"/></th>
                    <th scope="col"><fmt:message key="col.name"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${t_auto_models}">
                    <tr class="border-bottom">
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="m-3">
            <p><fmt:message key="order.status"/></p>
            <table class="table table-striped table-hover">
                <thead class="table-success">
                <tr>
                    <th scope="col"><fmt:message key="col.id"/></th>
                    <th scope="col"><fmt:message key="col.name"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${t_service_statuses}">
                    <tr class="border-bottom">
                        <td>${item.id}</td>
                        <td>${item.name}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</main>
<jsp:include page="bottom.jsp"/>
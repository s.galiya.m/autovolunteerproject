let pwd = document.getElementById('inputPassword'),
    pwdConfirm = document.getElementById('inputPasswordConfirm');
function validatePassword() {
    if (pwd.value !== pwdConfirm.value) {
        pwd.setCustomValidity("Passwords Don't Match");
    } else {
        pwd.setCustomValidity("");
    }
}

if (pwd != null && pwdConfirm != null) {
    pwd.onchange = validatePassword;
    pwdConfirm.onkeyup = validatePassword;
}

function setRequiredExtraFields() {
    let value = document.getElementById("selectUserType").value;
    document.getElementById('driverAccount').style.display = value == 'driver' ? 'block' : 'none';
    document.getElementById('inputDriverID').required = value == 'driver' ? true : false;
    document.getElementById('inputAutoID').required = value == 'driver' ? true : false;
    document.getElementById('selectAutoType').required = value == 'driver' ? true : false;
    document.getElementById('selectAutoModel').required = value == 'driver' ? true : false;

    document.getElementById('recipientAccount').style.display = value == 'recipient' ? 'block' : 'none';
    document.getElementById('selectDisType').required = value == 'recipient' ? true : false;

}

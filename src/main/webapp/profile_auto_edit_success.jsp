<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="auto.edit" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>
<main>
    <h2 class="my-4 text-primary">
        <fmt:message key="profile.msg"/>
    </h2>
    <a href="main" class="btn btn-md btn-primary fw-bold bg-gradient"><fmt:message key="to.main"/></a>

</main>
<jsp:include page="bottom.jsp"/>

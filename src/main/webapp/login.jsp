<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="signin" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>

    <main class="form-sign-in">
        <c:choose>
            <c:when test="${error_msg == 'no_user'}">
                <p class="text-danger mb-4"><fmt:message key="login.err"/></p>
            </c:when>
            <c:when test="${error_msg  == 'is_blocked'}">
                <p class="text-danger mb-4"><fmt:message key="login.block"/></p>
            </c:when>
        </c:choose>
        <c:remove var="error_msg" scope="session"/>
        <form action="main" method="post">
            <h1 class="h3 mb-3 fw-normal"><fmt:message key="signin"/></h1>
            <div>
                <label for="inputEmail" class="visually-hidden"></label>
                <input type="email" id="inputEmail" name="email" class="form-control"
                       pattern="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$" placeholder="<fmt:message key="email"/>" required autofocus/>
                <label for="inputPassword" class="visually-hidden"></label>
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="<fmt:message key="pwd"/>" required/>
            </div>
            <button type="submit" class="w-100 btn btn-lg btn-success mt-3 bg-gradient"><fmt:message key="login"/></button>
        </form>
            <p class="mt3"><fmt:message key="signup.msg"/></p>

        <a href="signup" class="btn btn-sm btn-primary fw-bold bg-gradient"><fmt:message key="signup"/></a>

    </main>

<jsp:include page="bottom.jsp"/>



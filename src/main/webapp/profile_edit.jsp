<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<fmt:message key="profile.edit" var="title"/>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="${title}"/>
</jsp:include>
<main>
    <form action="profile_edit_success" method="post" class="d-flex flex-column align-items-center">
        <h3 class="my-4 ms-5 text-primary"><fmt:message key="profile.edit"/> ${sessionScope.login}</h3>
        <div class="form-content ms-5">
            <label for="inputName" class="d-flex pb-3"> <span class="text-danger">*</span><span class="label-title"><fmt:message key="firstname"/></span>
                <input type="text" id="inputName" name="firstname" class="form-control ms-2" maxlength="255" value="${user.firstName}" required autofocus/></label>

            <label for="inputLastName" class="d-flex pb-3"> <span class="text-danger">*</span><span class="label-title"><fmt:message key="lastname"/></span>
                <input type="text" id="inputLastName" name="lastname" class="form-control ms-2" maxlength="255" value="${user.lastName}" required/></label>

            <label for="inputPhone" class="d-flex pb-3"> <span class="text-danger">*</span><span class="label-title"><fmt:message key="order.phone"/></span>
                <input type="tel" id="inputPhone" name="phone"  class="form-control ms-2" value="${user.phone}" minlength="12" maxlength="12" pattern="(\+?\d[- .]*){11}" required/></label>

        </div>
        <div class="d-flex">
            <a href="profile" class="btn btn-lg btn-secondary text-light mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="cancel"/></a>
            <button type="submit" class="btn btn-lg btn-success mt-5 ms-5 bg-gradient align-self-center"><fmt:message key="update"/></button>
        </div>
    </form>
</main>
<jsp:include page="bottom.jsp"/>
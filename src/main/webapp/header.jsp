<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="locale"/>

<html lang="${sessionScope.locale}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>${param.title}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body class="d-flex text-center bg-light bg-gradient">
    <div class="cover-container d-flex w-100 mx-auto flex-column">
        <header class="mb-auto p-3 text-white">
            <div class="d-flex align-items-center justify-content-between flex-wrap">

                <a href="/" class="navbar-brand d-flex align-items-center">
                    <img src="images/Car3.png" class="logo me-3" alt="">
                    <div class="text-white"><fmt:message key="name"/></div>
                </a>

                <nav class="nav">

                    <c:choose>
                        <c:when test="${session_id!=null}">
                            <span class="me-3 text-dark bg-light rounded py-2 px-3">
                                <c:choose>
                                    <c:when test="${sessionScope.is_driver}"><fmt:message key="driver"/></c:when>
                                    <c:when test="${sessionScope.is_recipient}"><fmt:message key="service.user"/></c:when>
                                    <c:when test="${sessionScope.is_admin}"><fmt:message key="admin"/></c:when>
                                </c:choose>
                            </span>
                            <c:if test="${sessionScope.is_admin}">
                                <a href="/users" class="btn me-3 text-light"><fmt:message key="users"/></a>
                                <a href="/reference_tables" class="btn me-3 text-light"><fmt:message key="references"/></a>
                            </c:if>
                            <a href="/main" class="btn me-3 text-light"><fmt:message key="main"/></a>
                            <a href="/profile" class="btn me-3 text-light">${sessionScope.login}</a>
                            <a href="/logout" class="btn btn-outline-light me-3" ><fmt:message key="logout"/></a>
                        </c:when>
                        <c:otherwise>
                            <a class="btn btn-outline-light me-3" href="/login"><fmt:message key="login"/></a>
                        </c:otherwise>
                    </c:choose>

                    <form action="" method="post" class="m-0">
                        <select onchange="this.form.submit()" name="lang" class="btn border py-2 text-light">
                            <c:forEach var="lang" items="${languages}">
                                <option value="${lang.code}" class="text-success"
                                        <c:choose>
                                            <c:when test="${sessionScope.locale!=null}">
                                                <c:if test="${lang.code == sessionScope.locale}">selected style="display: none"</c:if>
                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${lang.code == default_locale}">selected style="display: none"</c:if>
                                            </c:otherwise>
                                        </c:choose>
                                >
                                        ${lang.name}
                                </option>
                            </c:forEach>
                        </select>
                    </form>
                </nav>
            </div>
        </header>
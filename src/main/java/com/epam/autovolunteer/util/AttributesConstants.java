package com.epam.autovolunteer.util;

public class AttributesConstants {
    public static final String CITIES = "cities";
    public static final String AUTO_TYPES = "auto_types";
    public static final String AUTO_MODELS = "auto_models";
    public static final String DIS_TYPES = "dis_types";
    public static final String DIS_GROUPS = "dis_groups";
    public static final String SERVICE_STATUSES = "service_statuses";

    public static final String SESSION_ID = "session_id";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    public static final String LOGIN = "login";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String IS_ADMIN = "is_admin";
    public static final String IS_DRIVER = "is_driver";
    public static final String IS_RECIPIENT = "is_recipient";

    public static final String ERROR_MSG = "error_msg";

    public static final String CITY_CURRENT = "city_current";
    public static final String SERVICES_CREATED = "services_created";
    public static final String SERVICES_IN_WORK = "services_in_work";
    public static final String SERVICES_DONE = "services_done";

    public static final String CURRENT_DATETIME = "current_datetime";
    public static final String CURRENT_DATE= "current_date";
    public static final String CURRENT_TIME = "current_time";

    public static final String USERS = "users";
    public static final String RECIPIENTS = "recipients";
    public static final String RECIPIENT = "recipient";

    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String HELPER = "helper";
    public static final String ORDER_OWNER = "order_owner";

    public static final String ORDER_ID = "order_id";

    public static final String ORDER_RECIPIENT_ID = "order_recipient_id";
    public static final String ORDER_FROM = "order_from";
    public static final String ORDER_TO = "order_to";
    public static final String ORDER_DATE = "order_date";
    public static final String ORDER_TIME = "order_time";
    public static final String ORDER_HELPER = "order_helper";
    public static final String ORDER_CITY_ID = "order_city_id";
    public static final String DRIVERS = "drivers";
    public static final String DRIVER = "driver";
    public static final String ORDER_DRIVER = "order_driver";
    public static final String DRIVER_ID = "driver_id";

    public static final String AUTO_ID = "auto_id";
    public static final String AUTO = "auto";
    public static final String AUTO_TYPE = "auto_type";
    public static final String AUTO_MODEL = "auto_model";
    public static final String COLOR = "color";
    public static final String AUTO_COLOR = "auto_color";
    public static final String AUTO_DESC = "auto_desc";

    public static final String USER = "user";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String PHONE = "phone";
    public static final String RECIPIENT_ID = "recipient_id";
    public static final String DIS_TYPE = "dis_type";
    public static final String DIS_GR = "dis_gr";
    public static final String ADDRESS = "address";

    public static final String T_CITIES = "t_cities";
    public static final String T_AUTO_TYPES = "t_auto_types";
    public static final String T_AUTO_MODELS = "t_auto_models";
    public static final String T_DIS_TYPES = "t_dis_types";
    public static final String T_DIS_GROUPS = "t_dis_groups";
    public static final String T_SERVICE_STATUSES = "t_service_statuses";
    public static final String T_LANGUAGES = "t_languages";

    public static final String USER_TYPE = "user_type";
    public static final String SIGNUP_ERR = "signup_err";
    public static final String NAME = "name";
    public static final String CITY  = "city";
    public static final String USER_STATUS = "user_status";
    public static final String ALL_USERS = "all_users";























}

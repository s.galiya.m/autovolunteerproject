package com.epam.autovolunteer.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthorizationValidator {
    private static final String MAIL_PATTERN = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    private static final Pattern mailPattern = Pattern.compile(MAIL_PATTERN);

    public static boolean isValidMail(String email) {
        Matcher matcher = mailPattern.matcher(email);
        return matcher.matches();
    }
}

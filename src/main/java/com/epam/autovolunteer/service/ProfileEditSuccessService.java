package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.UserDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class ProfileEditSuccessService implements Service {
    UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {

            Long userId = (Long) session.getAttribute(USER_ID);

            String firstName = request.getParameter(FIRSTNAME);
            String lastName = request.getParameter(LASTNAME);
            String phone = request.getParameter(PHONE);

            if (firstName != null && lastName != null && phone != null) {
                userDao.update(firstName, lastName, phone, userId);

                dispatcher = request.getRequestDispatcher("/profile_edit_success.jsp");
                dispatcher.forward(request, response);
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

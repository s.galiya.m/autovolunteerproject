package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.DriverDaoImpl;
import com.epam.autovolunteer.dao.UserDaoImpl;
import com.epam.autovolunteer.entity.Driver;
import com.epam.autovolunteer.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class OrderTakeService implements Service{
    DriverDaoImpl driverDao = new DriverDaoImpl();
    UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession();

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {
            boolean isAdmin = (boolean) session.getAttribute(IS_ADMIN);
            boolean isDriver = (boolean) session.getAttribute(IS_DRIVER);

            if (isAdmin || isDriver) {
                String orderId = request.getParameter(ORDER_ID);

                if (orderId != null) {
                    if (isAdmin) {
                        Long orderCityId = Long.valueOf(request.getParameter(ORDER_CITY_ID));
                        request.setAttribute(ORDER_CITY_ID, orderCityId);

                        List<Driver> drivers = driverDao.getAllDriversByCity(orderCityId);
                        request.setAttribute(DRIVERS, drivers);

                        List<User> users = userDao.getAll();
                        request.setAttribute(USERS, users);

                        request.setAttribute(ORDER_ID, orderId);
                        dispatcher = request.getRequestDispatcher("/order_take.jsp");
                        dispatcher.forward(request, response);
                    } else {
                        session.setAttribute(ORDER_ID, orderId);
                        response.sendRedirect("/order_take_success");
                    }
                } else {
                    response.sendRedirect("/main");
                }
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

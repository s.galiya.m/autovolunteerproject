package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.*;
import com.epam.autovolunteer.entity.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class ReferenceTablesService implements Service{
    CityDaoImpl cityDao = new CityDaoImpl();
    AutoTypeDaoImpl autoTypeDao = new AutoTypeDaoImpl();
    AutoModelDaoImpl autoModelDao = new AutoModelDaoImpl();
    DisabilityTypeDaoImpl disabilityTypeDao = new DisabilityTypeDaoImpl();
    DisabilityGroupDaoImpl disabilityGroupDao = new DisabilityGroupDaoImpl();
    ServiceStatusDaoImpl serviceStatusDao = new ServiceStatusDaoImpl();
    LanguageDaoImpl languageDao = new LanguageDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {
            boolean isAdmin = (boolean) session.getAttribute(IS_ADMIN);
            if (isAdmin) {
                List<City> cities = cityDao.getAll();
                List<AutoType> autoTypes = autoTypeDao.getAll();
                List<AutoModel> autoModels = autoModelDao.getAll();
                List<DisabilityType> disabilityTypes = disabilityTypeDao.getAll();
                List<DisabilityGroup> disabilityGroups = disabilityGroupDao.getAll();
                List<ServiceStatus> serviceStatuses = serviceStatusDao.getAll();
                List<Language> languages = languageDao.getAll();

                request.setAttribute(T_CITIES, cities);
                request.setAttribute(T_AUTO_TYPES, autoTypes);
                request.setAttribute(T_AUTO_MODELS, autoModels);
                request.setAttribute(T_DIS_TYPES, disabilityTypes);
                request.setAttribute(T_DIS_GROUPS, disabilityGroups);
                request.setAttribute(T_SERVICE_STATUSES, serviceStatuses);
                request.setAttribute(T_LANGUAGES, languages);

                dispatcher = request.getRequestDispatcher("/reference_tables.jsp");
                dispatcher.forward(request, response);
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.AutoDaoImpl;
import com.epam.autovolunteer.entity.Auto;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class ProfileAutoEditService implements Service {
    AutoDaoImpl autoDao = new AutoDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {

            boolean isDriver = (boolean) session.getAttribute(IS_DRIVER);
            String autoId = request.getParameter(AUTO_ID);

            if (isDriver && autoId != null) {
                Auto auto = autoDao.getById(autoId);
                request.setAttribute(AUTO, auto);

                dispatcher = request.getRequestDispatcher("/profile_auto_edit.jsp");
                dispatcher.forward(request, response);
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.RecipientDaoImpl;
import com.epam.autovolunteer.dao.ServiceDaoImpl;
import com.epam.autovolunteer.dao.UserDaoImpl;
import com.epam.autovolunteer.entity.Recipient;
import com.epam.autovolunteer.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class OrderEditSuccessService implements Service {
    ServiceDaoImpl serviceDao = new ServiceDaoImpl();
    RecipientDaoImpl recipientDao = new RecipientDaoImpl();
    UserDaoImpl userDao = new UserDaoImpl();


    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession();

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {
            boolean isAdmin = (boolean) session.getAttribute(IS_ADMIN);
            boolean isRecipient = (boolean) session.getAttribute(IS_RECIPIENT);

            if (isAdmin || isRecipient) {

                String idString = request.getParameter(ORDER_ID);

                if (idString != null) {
                    Long id = Long.valueOf(idString);
                    Long recipientId = Long.valueOf(request.getParameter(RECIPIENT));
                    String from = request.getParameter(FROM);
                    String to = request.getParameter(TO);
                    Date date = Date.valueOf(request.getParameter(DATE));
                    String dateStr = request.getParameter(TIME) + ":00";
                    Time time = Time.valueOf(dateStr);
                    boolean withHelper = Boolean.parseBoolean(request.getParameter(HELPER));

                    serviceDao.update(id, recipientId, from, to, date, time, withHelper);

                    if (isAdmin) {
                        Recipient recipient = recipientDao.getById(recipientId);
                        User user = userDao.getById(recipient.getUserId());

                        request.setAttribute(ORDER_OWNER, user.getFirstName() + " " + user.getLastName());
                    }

                    request.setAttribute(ORDER_ID, idString);

                    dispatcher = request.getRequestDispatcher("/order_edit_success.jsp");
                    dispatcher.forward(request, response);
                }
                else {
                    response.sendRedirect("/main");
                }
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

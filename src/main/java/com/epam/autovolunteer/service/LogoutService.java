package com.epam.autovolunteer.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.autovolunteer.util.AttributesConstants.SESSION_ID;

public class LogoutService implements Service {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();

        String id = (String) session.getAttribute(SESSION_ID);
        if (id != null) {
            session.removeAttribute(SESSION_ID);
            session.invalidate();
        }

        response.sendRedirect("/");
    }
}

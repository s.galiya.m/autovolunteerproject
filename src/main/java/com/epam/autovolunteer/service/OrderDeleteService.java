package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.ServiceDaoImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class OrderDeleteService implements Service {
    ServiceDaoImpl serviceDao = new ServiceDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        HttpSession session = request.getSession();

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {
            boolean isAdmin = (boolean) session.getAttribute(IS_ADMIN);
            boolean isRecipient = (boolean) session.getAttribute(IS_RECIPIENT);

            if (isAdmin || isRecipient) {
                String idString = request.getParameter(ORDER_ID);

                if (idString != null) {
                    Long id = Long.valueOf(request.getParameter(ORDER_ID));
                    serviceDao.delete(id);

                    response.sendRedirect("/main");
                } else {
                    response.sendRedirect("/main");
                }
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

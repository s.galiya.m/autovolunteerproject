package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.UserDaoImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class UserBlockService implements Service {
    UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {
            boolean isAdmin = (boolean) session.getAttribute(IS_ADMIN);
            if (isAdmin) {

                Long userId = Long.valueOf(request.getParameter(USER_ID));
                Boolean status = Boolean.valueOf(request.getParameter(USER_STATUS));
                Boolean isBlocked = (status) ? false : true;

                userDao.updateUserStatus(userId, isBlocked);

                response.sendRedirect("/users");
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

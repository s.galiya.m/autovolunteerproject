package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.DriverDaoImpl;
import com.epam.autovolunteer.dao.ServiceDaoImpl;
import com.epam.autovolunteer.dao.UserDaoImpl;
import com.epam.autovolunteer.entity.Driver;
import com.epam.autovolunteer.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class OrderTakeSuccessService implements Service {
    ServiceDaoImpl serviceDao = new ServiceDaoImpl();
    DriverDaoImpl driverDao = new DriverDaoImpl();
    UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession();

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {
            boolean isAdmin = (boolean) session.getAttribute(IS_ADMIN);
            boolean isDriver = (boolean) session.getAttribute(IS_DRIVER);

            if (isAdmin || isDriver) {
                String idString = request.getParameter(ORDER_ID);
                String idStringFromSession = (String) session.getAttribute(ORDER_ID);

                if (idString != null && isAdmin) {
                    Long id = Long.valueOf(idString);
                    String driverId = request.getParameter(DRIVER);

                    Driver driver = driverDao.getById(driverId);
                    User user = userDao.getById(driver.getUserId());

                    request.setAttribute(ORDER_DRIVER, user.getFirstName() + " " + user.getLastName());
                    request.setAttribute(ORDER_ID, id);
                    request.setAttribute(DRIVER_ID, driverId);

                    Long orderInWorkStatus = Long.valueOf(2);

                    serviceDao.updateDriverAndStatus(driverId, orderInWorkStatus, id);

                    dispatcher = request.getRequestDispatcher("/order_take_success.jsp");
                    dispatcher.forward(request, response);
                } else if (idStringFromSession != null && isDriver) {
                    Long id = Long.valueOf(idStringFromSession);
                    Long userId = (Long) session.getAttribute(USER_ID);
                    Driver driver  =  driverDao.getByUserId(userId);
                    String driverId = driver.getId();

                    Long orderInWorkStatus = Long.valueOf(2);

                    serviceDao.updateDriverAndStatus(driverId, orderInWorkStatus, id);

                    dispatcher = request.getRequestDispatcher("/order_take_success.jsp");
                    dispatcher.forward(request, response);
                } else {
                    response.sendRedirect("/main");
                }
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

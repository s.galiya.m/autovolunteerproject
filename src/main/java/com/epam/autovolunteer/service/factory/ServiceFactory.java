package com.epam.autovolunteer.service.factory;

import com.epam.autovolunteer.service.*;

import java.util.HashMap;
import java.util.Map;

public class ServiceFactory {
    private static final Map<String, Service> SERVICE_MAP = new HashMap<>();
    private static final ServiceFactory SERVICE_FACTORY = new ServiceFactory();

    private ServiceFactory() {}

    static {
        SERVICE_MAP.put("/INDEX", new IndexService());
        SERVICE_MAP.put("/LOGIN", new LoginService());
        SERVICE_MAP.put("/SIGNUP" , new SignUpService());
        SERVICE_MAP.put("/SIGNUP_SUCCESS" , new SignUpSuccessService());
        SERVICE_MAP.put("/MAIN", new MainService());
        SERVICE_MAP.put("/LOGOUT", new LogoutService());
        SERVICE_MAP.put("/ORDER_ADD", new OrderAddService());
        SERVICE_MAP.put("/ORDER_ADD_SUCCESS", new OrderAddSuccessService());
        SERVICE_MAP.put("/ORDER_DELETE", new OrderDeleteService());
        SERVICE_MAP.put("/ORDER_TAKE", new OrderTakeService());
        SERVICE_MAP.put("/ORDER_TAKE_SUCCESS", new OrderTakeSuccessService());
        SERVICE_MAP.put("/ORDER_DONE", new OrderDoneService());
        SERVICE_MAP.put("/ORDER_EDIT", new OrderEditService());
        SERVICE_MAP.put("/ORDER_EDIT_SUCCESS", new OrderEditSuccessService());
        SERVICE_MAP.put("/PROFILE", new ProfileService());
        SERVICE_MAP.put("/PROFILE_EDIT", new ProfileEditService());
        SERVICE_MAP.put("/PROFILE_EDIT_SUCCESS", new ProfileEditSuccessService());
        SERVICE_MAP.put("/PROFILE_AUTO_EDIT", new ProfileAutoEditService());
        SERVICE_MAP.put("/PROFILE_AUTO_EDIT_SUCCESS", new ProfileAutoEditSuccessService());
        SERVICE_MAP.put("/PROFILE_RECIPIENT_EDIT", new ProfileRecipientEditService());
        SERVICE_MAP.put("/PROFILE_RECIPIENT_EDIT_SUCCESS", new ProfileRecipientEditSuccessService());
        SERVICE_MAP.put("/USERS", new UsersService());
        SERVICE_MAP.put("/USER_BLOCK", new UserBlockService());
        SERVICE_MAP.put("/REFERENCE_TABLES", new ReferenceTablesService());
        SERVICE_MAP.put("/ERROR", new ErrorService());
    }

    public Service getService(String request) {
        Service service = SERVICE_MAP.get("/ERROR");

        for (Map.Entry<String, Service> pair: SERVICE_MAP.entrySet()) {
            if (request.equalsIgnoreCase(pair.getKey())) {
                service = SERVICE_MAP.get(pair.getKey());
            }
        }
        return service;
    }

    public static ServiceFactory getInstance() {
       return SERVICE_FACTORY;
    }
}

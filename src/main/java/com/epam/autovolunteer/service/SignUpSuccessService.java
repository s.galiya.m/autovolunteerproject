package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.*;
import com.epam.autovolunteer.entity.Auto;
import com.epam.autovolunteer.entity.Driver;
import com.epam.autovolunteer.entity.User;
import com.epam.autovolunteer.validator.AuthorizationValidator;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class SignUpSuccessService implements Service {
    UserDaoImpl userDao = new UserDaoImpl();
    AutoDaoImpl autoDao = new AutoDaoImpl();
    DriverDaoImpl driverDao = new DriverDaoImpl();
    DriverAutoDaoImpl driverAutoDao = new DriverAutoDaoImpl();
    RecipientDaoImpl recipientDao = new RecipientDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession();

        String email = request.getParameter(EMAIL);

        if (email != null) {

            boolean isValidEmail = AuthorizationValidator.isValidMail(email.toUpperCase());

            if (isValidEmail) {
                if (isEmailUnique(email.toUpperCase())) {
                    String accountType = request.getParameter(USER_TYPE);
                    boolean isDriver = accountType.equals(DRIVER) ? true : false;
                    boolean isRecipient = accountType.equals(RECIPIENT) ? true : false;

                    if (isDriver) {
                        String autoId = request.getParameter(AUTO_ID);
                        String driverId = request.getParameter(DRIVER_ID);

                        boolean isAutoUnique = isAutoUnique(autoId);
                        boolean isDriverUnique = isDriverUnique(driverId);

                        if (!isAutoUnique || !isDriverUnique) {
                            session.setAttribute(SIGNUP_ERR, "auto_and_driver_exist");
                            if (!isAutoUnique && isDriverUnique) {
                                session.setAttribute(SIGNUP_ERR, "auto_exist");
                            } else if (isAutoUnique && !isDriverUnique) {
                                session.setAttribute(SIGNUP_ERR, "driver_exist");
                            }
                            response.sendRedirect("/signup");
                        } else {
                            User user = addUser(request, email, isDriver, isRecipient);

                            Long autoType = Long.valueOf(request.getParameter(AUTO_TYPE));
                            Long autoModel = Long.valueOf(request.getParameter(AUTO_MODEL));
                            String autoColor = request.getParameter(AUTO_COLOR);
                            autoColor = autoColor.equals("") ? null : autoColor;
                            String autoDesc = request.getParameter(AUTO_DESC);
                            autoDesc = autoDesc.equals("") ? null : autoDesc;

                            autoDao.add(autoId, autoType, autoModel, autoColor, autoDesc);
                            driverDao.add(driverId, user.getId());
                            driverAutoDao.add(driverId, autoId);
                            request.setAttribute(LOGIN, email);
                            dispatcher = request.getRequestDispatcher("/signup_success.jsp");
                            dispatcher.forward(request, response);
                        }
                    } else if (isRecipient) {
                        User user = addUser(request, email, isDriver, isRecipient);

                        Long disabilityType = Long.valueOf(request.getParameter(DIS_TYPE));
                        String disabilityGroup = request.getParameter(DIS_GR);
                        disabilityGroup = disabilityGroup.equals("") ? null : disabilityGroup;

                        String address = request.getParameter(ADDRESS);
                        address = address.equals("") ? null : address;

                        recipientDao.add(user.getId(), disabilityType, disabilityGroup != null ? Long.valueOf(disabilityGroup) : null, address);
                        request.setAttribute(LOGIN, email);
                        dispatcher = request.getRequestDispatcher("/signup_success.jsp");
                        dispatcher.forward(request, response);
                    } else {
                        addUser(request, email, isDriver, isRecipient);
                        request.setAttribute(LOGIN, email);
                        dispatcher = request.getRequestDispatcher("/signup_success.jsp");
                        dispatcher.forward(request, response);
                    }
                } else {
                    session.setAttribute(SIGNUP_ERR, "email_exist");
                    response.sendRedirect("/signup");
                }
            } else {
                session.setAttribute(SIGNUP_ERR, "email_not_valid");
                response.sendRedirect("/signup");
            }
        } else {
            response.sendRedirect("/");
        }
    }

    private User addUser(HttpServletRequest request, String email, boolean isDriver, boolean isRecipient) throws IOException, SQLException {
        String name = request.getParameter(NAME);
        String lastName = request.getParameter(LASTNAME);
        String phone = request.getParameter(PHONE);
        String password = StringEscapeUtils.escapeJava(request.getParameter(PASSWORD));
        String hashPassword = DigestUtils.md5Hex(password);
        String cityId = request.getParameter(CITY);

        userDao.add(name, lastName, phone, email, hashPassword, cityId, isDriver, isRecipient);

        User user = userDao.getByEmailAndPassword(email, hashPassword);
        return user;
    }

    private boolean isEmailUnique(String email) throws SQLException {
        List<String> emails = userDao.getEmails();
        if (emails.size()!=0) {
            for (String item : emails) {
                if (item.toUpperCase().equals(email)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isDriverUnique(String driverId) throws SQLException {
        Driver driver = driverDao.getById(driverId);
        if (driver != null) {
            return false;
        }
        return true;
    }

    private boolean isAutoUnique(String autoId) throws SQLException {
        Auto auto = autoDao.getById(autoId);
        if (auto != null) {
            return false;
        }
        return true;
    }
}

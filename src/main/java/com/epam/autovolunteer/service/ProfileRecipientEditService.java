package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.RecipientDaoImpl;
import com.epam.autovolunteer.entity.Recipient;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class ProfileRecipientEditService implements Service {
    RecipientDaoImpl recipientDao = new RecipientDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {

            boolean isRecipient = (boolean) session.getAttribute(IS_RECIPIENT);
            String recipientId = request.getParameter(RECIPIENT_ID);

            if (isRecipient && recipientId != null) {
                Recipient recipient = recipientDao.getById(Long.valueOf(recipientId));
                request.setAttribute(RECIPIENT, recipient);

                dispatcher = request.getRequestDispatcher("/profile_recipient_edit.jsp");
                dispatcher.forward(request, response);
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

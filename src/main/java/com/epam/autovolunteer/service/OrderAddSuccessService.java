package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.RecipientDaoImpl;
import com.epam.autovolunteer.dao.ServiceDaoImpl;
import com.epam.autovolunteer.dao.UserDaoImpl;
import com.epam.autovolunteer.entity.Recipient;
import com.epam.autovolunteer.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class OrderAddSuccessService implements Service {
    ServiceDaoImpl serviceDao = new ServiceDaoImpl();
    RecipientDaoImpl recipientDao = new RecipientDaoImpl();
    UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession();

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {

            boolean isAdmin = (boolean) session.getAttribute(IS_ADMIN);
            boolean isRecipient = (boolean) session.getAttribute(IS_RECIPIENT);

            if (isAdmin || isRecipient) {
                String recipientString = request.getParameter(RECIPIENT);
                if (recipientString != null) {
                    Long recipientId = Long.valueOf(recipientString);
                    String from = request.getParameter(FROM);
                    String to = request.getParameter(TO);
                    Date date = Date.valueOf(request.getParameter(DATE));
                    String dateStr = request.getParameter(TIME) + ":00";
                    Time time = Time.valueOf(dateStr);

                    boolean withHelper = Boolean.parseBoolean(request.getParameter(HELPER));

                    serviceDao.add(recipientId, from, to, date, time, withHelper);

                    Recipient recipient = recipientDao.getById(recipientId);
                    User user = userDao.getById(recipient.getUserId());

                    request.setAttribute(ORDER_OWNER, user.getFirstName() + " " + user.getLastName());

                    dispatcher = request.getRequestDispatcher("/order_add_success.jsp");
                    dispatcher.forward(request, response);
                } else {
                    response.sendRedirect("/main");
                }
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

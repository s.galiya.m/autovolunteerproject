package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.RecipientDaoImpl;
import com.epam.autovolunteer.dao.UserDaoImpl;
import com.epam.autovolunteer.entity.Recipient;
import com.epam.autovolunteer.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class OrderEditService implements Service{
    RecipientDaoImpl recipientDao = new RecipientDaoImpl();
    UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession();

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {
            boolean isAdmin = (boolean) session.getAttribute(IS_ADMIN);
            boolean isRecipient = (boolean) session.getAttribute(IS_RECIPIENT);

            if (isAdmin || isRecipient) {

                String orderId = request.getParameter(ORDER_ID);

                if (orderId != null) {

                    if (isAdmin) {
                        List<Recipient> recipients = recipientDao.getAll();
                        request.setAttribute(RECIPIENTS, recipients);

                        List<User> users = userDao.getAll();
                        request.setAttribute(USERS, users);
                    }

                    request.setAttribute(ORDER_ID, orderId);
                    request.setAttribute(ORDER_RECIPIENT_ID, request.getParameter(ORDER_RECIPIENT_ID));
                    request.setAttribute(ORDER_FROM, request.getParameter(ORDER_FROM));
                    request.setAttribute(ORDER_TO, request.getParameter(ORDER_TO));
                    request.setAttribute(ORDER_DATE, request.getParameter(ORDER_DATE));
                    request.setAttribute(ORDER_TIME, request.getParameter(ORDER_TIME));
                    request.setAttribute(ORDER_HELPER, request.getParameter(ORDER_HELPER));

                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("HH:mm");
                    LocalDateTime now = LocalDateTime.now();

                    request.setAttribute(CURRENT_DATE, dtf.format(now));
                    request.setAttribute(CURRENT_TIME, dtf2.format(now));

                    dispatcher = request.getRequestDispatcher("/order_edit.jsp");
                    dispatcher.forward(request, response);
                } else {
                    response.sendRedirect("/main");
                }
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

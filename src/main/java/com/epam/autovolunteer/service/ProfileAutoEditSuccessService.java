package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.AutoDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class ProfileAutoEditSuccessService implements Service {
    AutoDaoImpl autoDao = new AutoDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {

            boolean isDriver = (boolean) session.getAttribute(IS_DRIVER);

            if (isDriver) {
                String autoId = request.getParameter(AUTO_ID);

                String autoType = request.getParameter(AUTO_TYPE);
                String autoModel = request.getParameter(AUTO_MODEL);
                String color = request.getParameter(COLOR);
                color = color.equals("") ? null : color;
                String desc = request.getParameter(AUTO_DESC);
                desc = desc.equals("") ? null : desc;

                if (autoType != null && autoModel != null && autoId != null) {
                    autoDao.update(Long.valueOf(autoType), Long.valueOf(autoModel), color, desc, autoId);

                    dispatcher = request.getRequestDispatcher("/profile_auto_edit_success.jsp");
                    dispatcher.forward(request, response);

                } else {
                    response.sendRedirect("/main");
                }
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.AutoDaoImpl;
import com.epam.autovolunteer.dao.DriverDaoImpl;
import com.epam.autovolunteer.dao.RecipientDaoImpl;
import com.epam.autovolunteer.dao.UserDaoImpl;
import com.epam.autovolunteer.entity.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class ProfileService implements Service {
    UserDaoImpl userDao = new UserDaoImpl();
    RecipientDaoImpl recipientDao = new RecipientDaoImpl();
    DriverDaoImpl driverDao = new DriverDaoImpl();
    AutoDaoImpl autoDao = new AutoDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {

            boolean isDriver = (boolean) session.getAttribute(IS_DRIVER);
            boolean isRecipient = (boolean) session.getAttribute(IS_RECIPIENT);

            Long userId = (Long) session.getAttribute(USER_ID);

            if (isDriver) {
                Driver driver = driverDao.getByUserId(userId);
                request.setAttribute(DRIVER, driver);

                Auto auto = autoDao.getByDriverId(driver.getId());
                request.setAttribute(AUTO, auto);
            }

            if (isRecipient) {
                Recipient recipient = recipientDao.getByUserId(userId);
                request.setAttribute(RECIPIENT, recipient);
            }

            User user = userDao.getById(userId);
            request.setAttribute(USER, user);

            ServletContext servletContext = request.getServletContext();
            List<City> cities = (List<City>) servletContext.getAttribute("cities");
            List<City> currentUserCity = new ArrayList<>();

            for (City city : cities) {
                if (city.getId() == user.getCityId()) {
                    currentUserCity.add(city);
                }
            }

            session.setAttribute(CITY_CURRENT, currentUserCity);
            session.setAttribute(USER_NAME, user.getFirstName());

            dispatcher = request.getRequestDispatcher("/profile.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("/");
        }
    }
}

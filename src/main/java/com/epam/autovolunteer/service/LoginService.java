package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.*;
import com.epam.autovolunteer.entity.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class LoginService implements Service {
    CityDaoImpl cityDao = new CityDaoImpl();
    AutoTypeDaoImpl autoTypeDao = new AutoTypeDaoImpl();
    AutoModelDaoImpl autoModelDao = new AutoModelDaoImpl();
    DisabilityTypeDaoImpl disabilityTypeDao = new DisabilityTypeDaoImpl();
    DisabilityGroupDaoImpl disabilityGroupDao = new DisabilityGroupDaoImpl();
    ServiceStatusDaoImpl serviceStatusDao = new ServiceStatusDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(false);

        String sessionId = (String) session.getAttribute("session_id");

        if (sessionId == null) {
            List<City> cities = cityDao.getAll();
            List<AutoType> autoTypes = autoTypeDao.getAll();
            List<AutoModel> autoModels = autoModelDao.getAll();
            List<DisabilityType> disabilityTypes = disabilityTypeDao.getAll();
            List<DisabilityGroup> disabilityGroups = disabilityGroupDao.getAll();
            List<ServiceStatus> serviceStatuses = serviceStatusDao.getAll();

            ServletContext servletContext = request.getServletContext();
            servletContext.setAttribute(CITIES, cities);
            servletContext.setAttribute(AUTO_TYPES, autoTypes);
            servletContext.setAttribute(AUTO_MODELS, autoModels);
            servletContext.setAttribute(DIS_TYPES, disabilityTypes);
            servletContext.setAttribute(DIS_GROUPS, disabilityGroups);
            servletContext.setAttribute(SERVICE_STATUSES, serviceStatuses);

            dispatcher = request.getRequestDispatcher("/login.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("/main");
        }
    }
}

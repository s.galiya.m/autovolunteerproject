package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.RecipientDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class ProfileRecipientEditSuccessService implements Service {
    RecipientDaoImpl recipientDao = new RecipientDaoImpl();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId != null) {

            boolean isRecipient = (boolean) session.getAttribute(IS_RECIPIENT);

            if (isRecipient) {
                String recipientId = request.getParameter(RECIPIENT_ID);

                String disType = request.getParameter(DIS_TYPE);
                String disGroup = request.getParameter(DIS_GR);
                disGroup = disGroup.equals("") ? null : disGroup;

                String address = request.getParameter(ADDRESS);
                address = address.equals("") ? null : address;

                if (disType != null && recipientId != null) {
                    recipientDao.update(Long.valueOf(disType), disGroup != null ? Long.valueOf(disGroup) : null, address, Long.valueOf(recipientId));

                    dispatcher = request.getRequestDispatcher("/profile_recipient_edit_success.jsp");
                    dispatcher.forward(request, response);

                } else {
                    response.sendRedirect("/main");
                }
            } else {
                response.sendRedirect("/main");
            }
        } else {
            response.sendRedirect("/");
        }
    }
}

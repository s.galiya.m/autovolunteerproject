package com.epam.autovolunteer.service;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.autovolunteer.util.AttributesConstants.SESSION_ID;

public class SignUpService implements Service {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession();

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId == null) {
            dispatcher = request.getRequestDispatcher("/signup.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("/main");
        }
    }
}

package com.epam.autovolunteer.service;

import com.epam.autovolunteer.dao.ServiceExtendedDaoImpl;
import com.epam.autovolunteer.dao.UserDaoImpl;
import com.epam.autovolunteer.entity.City;
import com.epam.autovolunteer.entity.ServiceExtended;
import com.epam.autovolunteer.entity.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.epam.autovolunteer.util.AttributesConstants.*;

public class MainService implements Service {
    UserDaoImpl userDao = new UserDaoImpl();
    ServiceExtendedDaoImpl serviceExtendedDao = new ServiceExtendedDaoImpl();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        RequestDispatcher dispatcher;

        HttpSession session = request.getSession(true);

        String sessionId = (String) session.getAttribute(SESSION_ID);

        if (sessionId == null) {
            String email = request.getParameter(EMAIL);
            String password = request.getParameter(PASSWORD);

            if (email != null && password != null) {

                String hashPassword = DigestUtils.md5Hex(password).toUpperCase();
                User user = userDao.getByEmailAndPassword(email, hashPassword);

                if (user != null) {
                    if (!user.isBlocked()) {
                        session.setAttribute(SESSION_ID, session.getId());
                        session.setAttribute(LOGIN, user.getEmail());
                        session.setAttribute(USER_ID, user.getId());
                        session.setAttribute(USER_NAME, user.getFirstName());
                        session.setAttribute(IS_ADMIN, user.isAdmin());
                        session.setAttribute(IS_DRIVER, user.isDriver());
                        session.setAttribute(IS_RECIPIENT, user.isRecipient());

                        prepareMain(request, user, session);
                        dispatcher = request.getRequestDispatcher("/main.jsp");
                        dispatcher.forward(request, response);
                    } else {
                        session.setAttribute(ERROR_MSG, "is_blocked");
                        response.sendRedirect("/login");
                    }
                } else {
                    session.setAttribute(ERROR_MSG, "no_user");
                    response.sendRedirect("/login");
                }
            } else {
                response.sendRedirect("/");
            }
        } else {
            User user = userDao.getById((Long) session.getAttribute(USER_ID));
            prepareMain(request, user, session);

            dispatcher = request.getRequestDispatcher("/main.jsp");
            dispatcher.forward(request, response);
        }

    }

    private void prepareMain(HttpServletRequest request, User user, HttpSession session) throws SQLException {
        ServletContext servletContext = request.getServletContext();
        List<City> cities = (List<City>) servletContext.getAttribute(CITIES);

        List<City> currentUserCity = new ArrayList<>();

        List<List<ServiceExtended>> servicesCreatedByCity = new ArrayList<>();
        List<List<ServiceExtended>> servicesInWorkByCity = new ArrayList<>();
        List<List<ServiceExtended>> servicesDoneByCity = new ArrayList<>();

        for (City city : cities) {
            if (city.getId() == user.getCityId()) {
                currentUserCity.add(city);
            }

            Long serviceCreated = Long.valueOf(1);
            Long serviceInWork = Long.valueOf(2);
            Long serviceDone = Long.valueOf(3);

            List<ServiceExtended> servicesCreated = serviceExtendedDao.getByStatusAndCity(serviceCreated, city.getId());
            List<ServiceExtended> servicesInWork = serviceExtendedDao.getByStatusAndCity(serviceInWork, city.getId());
            List<ServiceExtended> servicesDone = serviceExtendedDao.getByStatusAndCity(serviceDone, city.getId());

            servicesCreatedByCity.add(servicesCreated);
            servicesInWorkByCity.add(servicesInWork);
            servicesDoneByCity.add(servicesDone);
        }

        session.setAttribute(CITY_CURRENT, currentUserCity);
        session.setAttribute(SERVICES_CREATED, servicesCreatedByCity);
        session.setAttribute(SERVICES_IN_WORK, servicesInWorkByCity);
        session.setAttribute(SERVICES_DONE, servicesDoneByCity);

        LocalDateTime now = LocalDateTime.now();

        request.setAttribute(CURRENT_DATETIME, dtf.format(now));
    }
}

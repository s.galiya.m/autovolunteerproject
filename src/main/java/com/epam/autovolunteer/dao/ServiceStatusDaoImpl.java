package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.ServiceStatusDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.ServiceStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ServiceStatusDaoImpl implements ServiceStatusDao {
    private final static String GET_ALL_SERVICE_STATUS = "SELECT * FROM service_status";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<ServiceStatus> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<ServiceStatus> serviceStatuses = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_SERVICE_STATUS);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                ServiceStatus serviceStatus = new ServiceStatus();
                serviceStatus.setId(resultSet.getLong("id"));
                serviceStatus.setName(resultSet.getString("name"));
                serviceStatuses.add(serviceStatus);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return serviceStatuses;
    }
}

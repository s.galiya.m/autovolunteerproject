package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.CityDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.City;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CityDaoImpl implements CityDao {
    private final static String GET_ALL_CITY = "SELECT * FROM city ORDER BY id";
    private final static String GET_CITY_BY_ID = "SELECT * FROM city WHERE id = ?";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<City> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<City> cities = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_CITY);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                City city = new City();
                city.setId(resultSet.getLong("id"));
                city.setName(resultSet.getString("name"));
                cities.add(city);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return cities;
    }

    @Override
    public City getById(Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        City city = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_CITY_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                city = new City();
                city.setId(resultSet.getLong("id"));
                city.setName(resultSet.getString("name"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }

        return city;
    }
}

package com.epam.autovolunteer.dao.interfaces;

import java.sql.SQLException;
import java.util.List;

public interface IDaoBase<T> {
    List<T> getAll() throws SQLException;
}

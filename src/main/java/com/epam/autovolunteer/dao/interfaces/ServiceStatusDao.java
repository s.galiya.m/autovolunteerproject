package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.ServiceStatus;

import java.sql.SQLException;
import java.util.List;

public interface ServiceStatusDao extends IDaoBase<ServiceStatus> {
    List<ServiceStatus> getAll() throws SQLException;
}

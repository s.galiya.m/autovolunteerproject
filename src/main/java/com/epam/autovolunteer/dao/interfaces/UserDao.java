package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

public interface UserDao extends IDaoBase<User> {
//    List<User> getAll() throws SQLException;

    User getById(Long id) throws SQLException;

    void add(String name, String lastName, String phone, String email, String password, String cityId, boolean isDriver, boolean isRecipient) throws SQLException, IOException;

    void update(String firstName, String lastName, String phone, Long id) throws SQLException;

    void updateUserStatus(Long id, Boolean status) throws SQLException;

    User getByEmailAndPassword(String email, String password) throws SQLException, IOException;

    List<String> getEmails() throws SQLException;
}

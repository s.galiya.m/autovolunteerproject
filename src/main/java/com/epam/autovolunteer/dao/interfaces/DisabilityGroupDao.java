package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.DisabilityGroup;

import java.sql.SQLException;
import java.util.List;

public interface DisabilityGroupDao extends IDaoBase<DisabilityGroup> {
//    List<DisabilityGroup> getAll() throws SQLException;
}

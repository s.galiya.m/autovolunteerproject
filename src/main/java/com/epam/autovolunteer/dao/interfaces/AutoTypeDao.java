package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.AutoType;

import java.sql.SQLException;
import java.util.List;

public interface AutoTypeDao extends IDaoBase<AutoType> {
//    List<AutoType> getAll() throws SQLException;
}

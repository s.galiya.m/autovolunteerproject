package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.Service;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.util.List;

public interface ServiceDao extends IDaoBase<Service> {
//    List<Service> getAll() throws SQLException;

    void add(Long recipientId, String fromPlace, String toPlace, Date date, Time time, boolean withHelper) throws SQLException;

    void update(Long id, Long recipientId, String fromPlace, String toPlace, Date date, Time time, boolean withHelper) throws SQLException;

    void updateDriverAndStatus(String driverId, Long serviceStatusId, Long id) throws SQLException;

    void updateStatus(Long serviceStatusId, Long id) throws SQLException;

    void delete(Long id) throws SQLException;
}

package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.ServiceExtended;

import java.sql.SQLException;
import java.util.List;

public interface ServiceExtendedDao {
    List<ServiceExtended> getByStatusAndCity(Long serviceStatusId, Long cityId) throws SQLException;
}

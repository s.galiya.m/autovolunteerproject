package com.epam.autovolunteer.dao.interfaces;

import java.sql.SQLException;

public interface DriverAutoDao {
    void add(String driverId, String autoId) throws SQLException;
}

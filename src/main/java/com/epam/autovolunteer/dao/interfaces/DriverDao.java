package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.Driver;

import java.sql.SQLException;
import java.util.List;

public interface DriverDao extends IDaoBase<Driver> {
//    List<Driver> getAll() throws SQLException;

    Driver getById(String id) throws SQLException;

    List<Driver> getAllDriversByCity(Long cityId) throws SQLException;

    Driver getByUserId(Long id) throws SQLException;

    void add(String driverId, Long userId) throws SQLException;
}

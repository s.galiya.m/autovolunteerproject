package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.Recipient;

import java.sql.SQLException;
import java.util.List;

public interface RecipientDao extends IDaoBase<Recipient> {
//    List<Recipient> getAll() throws SQLException;

    Recipient getById(Long id) throws SQLException;

    Recipient getByUserId(Long userId) throws SQLException;

    void add(Long userId, Long disabilityTypeId, Long disabilityGroupId, String address) throws SQLException;

    void update(Long disType, Long disGroup, String address, Long recipientId) throws SQLException;
}

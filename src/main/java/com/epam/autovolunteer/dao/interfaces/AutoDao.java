package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.Auto;
import java.sql.SQLException;

public interface AutoDao {

    Auto getById(String id) throws SQLException;

    Auto getByDriverId(String driver_id) throws SQLException;

    void add(String autoId, Long autoType, Long autoModel, String autoColor, String autoDesc) throws SQLException;

    void update(Long autoType, Long autoModel, String autoColor, String autoDesc, String autoId) throws SQLException;
}

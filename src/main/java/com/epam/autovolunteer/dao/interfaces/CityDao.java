package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.City;

import java.sql.SQLException;
import java.util.List;

public interface CityDao extends IDaoBase<City> {
//    List<City> getAll() throws SQLException;

    City getById(Long id) throws SQLException;
}

package com.epam.autovolunteer.dao.interfaces;

import com.epam.autovolunteer.entity.DisabilityType;

import java.sql.SQLException;
import java.util.List;

public interface DisabilityTypeDao extends IDaoBase<DisabilityType> {
//    List<DisabilityType> getAll() throws SQLException;
}

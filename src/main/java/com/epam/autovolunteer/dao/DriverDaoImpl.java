package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.DriverDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.Driver;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DriverDaoImpl implements DriverDao {
    private final static String GET_ALL_DRIVER = "SELECT * FROM driver";
    private final static String GET_ALL_DRIVER_BY_CITY = "SELECT d.id, d.user_id FROM driver as d INNER JOIN users as u ON d.user_id = u.id WHERE u.city_id = ?";
    private final static String GET_DRIVER_BY_ID= "SELECT * FROM driver WHERE id = ?";
    private final static String GET_DRIVER_BY_USER_ID= "SELECT * FROM driver WHERE user_id = ?";
    private final static String ADD_DRIVER = "INSERT INTO driver (id, user_id) VALUES (?, ?)";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<Driver> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<Driver> drivers = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_DRIVER);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Driver driver = new Driver();
                driver.setId(resultSet.getString("id"));
                driver.setUserId(resultSet.getLong("user_id"));
                drivers.add(driver);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return drivers;
    }

    @Override
    public List<Driver> getAllDriversByCity(Long cityId) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        List<Driver> drivers = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_DRIVER_BY_CITY);
            preparedStatement.setLong(1, cityId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Driver driver = new Driver();
                driver.setId(resultSet.getString("id"));
                driver.setUserId(resultSet.getLong("user_id"));
                drivers.add(driver);
            }

        } finally {
            connectionPool.returnConnection(connection);
        }
        return drivers;
    }

    @Override
    public Driver getById(String id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        Driver driver = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_DRIVER_BY_ID);
            preparedStatement.setString(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                driver =  new Driver();
                driver.setId(resultSet.getString("id"));
                driver.setUserId(resultSet.getLong("user_id"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return driver;
    }

    @Override
    public void add(String driverId, Long userId) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_DRIVER);
            preparedStatement.setString(1, driverId);
            preparedStatement.setLong(2, userId);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public Driver getByUserId(Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        Driver driver = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_DRIVER_BY_USER_ID);
            preparedStatement.setLong(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                driver =  new Driver();
                driver.setId(resultSet.getString("id"));
                driver.setUserId(resultSet.getLong("user_id"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return driver;
    }
}

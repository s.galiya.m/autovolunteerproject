package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.DisabilityGroupDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.DisabilityGroup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DisabilityGroupDaoImpl implements DisabilityGroupDao {
    private final static String GET_ALL_DISABILITY_GROUP = "SELECT * FROM disability_group ORDER BY id";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<DisabilityGroup> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<DisabilityGroup> disabilityGroups = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_DISABILITY_GROUP);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                DisabilityGroup disabilityGroup = new DisabilityGroup();
                disabilityGroup.setId(resultSet.getLong("id"));
                disabilityGroup.setName(resultSet.getString("name"));
                disabilityGroups.add(disabilityGroup);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return disabilityGroups;
    }
}

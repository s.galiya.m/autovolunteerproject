package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.UserDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.User;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private final static String GET_ALL_USER = "SELECT * FROM users";
    private final static String GET_USER_BY_LOGIN_PASSWORD = "SELECT * FROM users WHERE email = ? AND password = ?";
    private final static String GET_USER_BY_ID = "SELECT * FROM users WHERE id = ?";
    private final static String ADD_USER = "INSERT INTO users (first_name, last_name, phone, email, password, city_id, is_driver, is_recipient) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private final static String UPDATE_USER = "UPDATE users SET first_name = ?, last_name = ?, phone = ? where id = ?";
    private final static String UPDATE_USER_STATUS_BY_ID = "UPDATE users SET is_blocked = ? where id = ?";
    private final static String GET_ALL_EMAILS = "SELECT email FROM users";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<User> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<User> users = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_USER);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong("id"));
                user.setFirstName(resultSet.getString("first_name"));
                user.setLastName(resultSet.getString("last_name"));
                user.setPhone(resultSet.getString("phone"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setCityId(resultSet.getLong("city_id"));
                user.setAdmin(resultSet.getBoolean("is_admin"));
                user.setDriver(resultSet.getBoolean("is_driver"));
                user.setRecipient(resultSet.getBoolean("is_recipient"));
                user.setBlocked(resultSet.getBoolean("is_blocked"));

                users.add(user);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return users;
    }

    @Override
    public User getById(Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        User user = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_USER_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getLong("id"));
                user.setFirstName(resultSet.getString("first_name"));
                user.setLastName(resultSet.getString("last_name"));
                user.setPhone(resultSet.getString("phone"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setCityId(resultSet.getLong("city_id"));
                user.setAdmin(resultSet.getBoolean("is_admin"));
                user.setDriver(resultSet.getBoolean("is_driver"));
                user.setRecipient(resultSet.getBoolean("is_recipient"));
                user.setBlocked(resultSet.getBoolean("is_blocked"));

            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return user;
    }

    @Override
    public void add(String name, String lastName, String phone, String email, String password, String cityId, boolean isDriver, boolean isRecipient) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {

            preparedStatement = connection.prepareStatement(ADD_USER);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3, phone);
            preparedStatement.setString(4, email);
            preparedStatement.setString(5, password);
            preparedStatement.setLong(6, Long.parseLong(cityId));
            preparedStatement.setBoolean(7, isDriver);
            preparedStatement.setBoolean(8, isRecipient);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void update(String firstName, String lastName, String phone, Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_USER);
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3, phone);
            preparedStatement.setLong(4, id);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void updateUserStatus(Long id, Boolean status) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_USER_STATUS_BY_ID);
            preparedStatement.setBoolean(1, status);
            preparedStatement.setLong(2, id);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public User getByEmailAndPassword(String email, String password) throws SQLException, IOException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        User user = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN_PASSWORD);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getLong("id"));
                user.setFirstName(resultSet.getString("first_name"));
                user.setLastName(resultSet.getString("last_name"));
                user.setPhone(resultSet.getString("phone"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setCityId(resultSet.getLong("city_id"));
                user.setAdmin(resultSet.getBoolean("is_admin"));
                user.setDriver(resultSet.getBoolean("is_driver"));
                user.setRecipient(resultSet.getBoolean("is_recipient"));
                user.setBlocked(resultSet.getBoolean("is_blocked"));

            }
        } finally {
            connectionPool.returnConnection(connection);
        }

        return user;
    }

    @Override
    public List<String> getEmails() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<String> emails = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_EMAILS);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                emails.add(resultSet.getString("email"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return emails;
    }
}

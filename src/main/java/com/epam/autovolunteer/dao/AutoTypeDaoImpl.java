package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.AutoTypeDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.AutoType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AutoTypeDaoImpl implements AutoTypeDao {
    private final static String GET_ALL_AUTO_TYPE = "SELECT * FROM auto_type ORDER BY id";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<AutoType> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<AutoType> autoTypes = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_AUTO_TYPE);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                AutoType autoType = new AutoType();
                autoType.setId(resultSet.getLong("id"));
                autoType.setName(resultSet.getString("name"));
                autoTypes.add(autoType);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return autoTypes;
    }
}

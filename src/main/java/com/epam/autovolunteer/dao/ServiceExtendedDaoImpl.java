package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.ServiceExtendedDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.ServiceExtended;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ServiceExtendedDaoImpl implements ServiceExtendedDao {
    private final static String GET_ALL_SERVICE_BY_STATUS_AND_CITY = "SELECT * FROM services_by_status_and_city WHERE service_status_id = ? AND city_id = ? ORDER BY id";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<ServiceExtended> getByStatusAndCity(Long serviceStatusId, Long cityId) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<ServiceExtended> services = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_SERVICE_BY_STATUS_AND_CITY);
            preparedStatement.setLong(1, serviceStatusId);
            preparedStatement.setLong(2, cityId);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                ServiceExtended service = new ServiceExtended();
                service.setId(resultSet.getLong("id"));
                service.setDriverId(resultSet.getString("driver_id"));
                service.setRecipientId(resultSet.getLong("recipient_id"));
                service.setFromPlace(resultSet.getString("from_place"));
                service.setToPlace(resultSet.getString("to_place"));
                service.setDate(resultSet.getDate("date"));
                service.setTime(resultSet.getTime("time"));
                service.setWithHelper(resultSet.getBoolean("with_helper"));
                service.setServiceStatusId(resultSet.getLong("service_status_id"));

                service.setFirstName(resultSet.getString("first_name"));
                service.setLastName(resultSet.getString("last_name"));
                service.setPhone(resultSet.getString("phone"));
                service.setEmail(resultSet.getString("email"));
                service.setCityId(resultSet.getLong("city_id"));

                service.setUserId(resultSet.getLong("user_id"));
                service.setDisabilityTypeId(resultSet.getLong("disability_type_id"));
                service.setDisabilityGroupId(resultSet.getLong("disability_group_id"));

                service.setDriverUserId(resultSet.getLong("driver_user_id"));
                service.setDriverFirstName(resultSet.getString("driver_first_name"));
                service.setDriverLastName(resultSet.getString("driver_last_name"));
                service.setDriverPhone(resultSet.getString("driver_phone"));

                service.setAutoId(resultSet.getString("auto_id"));
                service.setColor(resultSet.getString("color"));
                service.setAutoType(resultSet.getString("auto_type"));
                service.setAutoModel(resultSet.getString("auto_model"));

                services.add(service);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return services;
    }
}

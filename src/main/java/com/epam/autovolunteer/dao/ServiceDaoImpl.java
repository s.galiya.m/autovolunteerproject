package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.ServiceDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ServiceDaoImpl implements ServiceDao {
    private final static String GET_ALL_SERVICE = "SELECT * FROM service";
    private final static String GET_ALL_SERVICE_BY_STATUS_AND_CITY = "SELECT * FROM services_by_status_and_city WHERE service_status_id = ? AND city_id = ?";
    private final static String ADD_SERVICE = "INSERT INTO service (recipient_id, from_place, to_place, date, time, with_helper) VALUES (?, ?, ?, ?, ?, ?)";
    private final static String UPDATE_SERVICE_BY_ID = "UPDATE service SET recipient_id = ?, from_place = ?, to_place = ?, date = ?, time = ?, with_helper = ? where id = ?";
    private final static String UPDATE_SERVICE_DRIVER_STATUS_BY_ID = "UPDATE service SET driver_id = ?, service_status_id = ? where id = ?";
    private final static String UPDATE_SERVICE_STATUS_BY_ID = "UPDATE service SET service_status_id = ? where id = ?";
    private final static String DELETE_SERVICE_BY_ID = "DELETE FROM service where id = ?";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<Service> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<Service> services = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_SERVICE);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Service service = new Service();
                service.setId(resultSet.getLong("id"));
                service.setDriverId(resultSet.getString("driver_id"));
                service.setRecipientId(resultSet.getLong("recipient_id"));
                service.setFromPlace(resultSet.getString("from_place"));
                service.setToPlace(resultSet.getString("to_place"));
                service.setDate(resultSet.getDate("date"));
                service.setTime(resultSet.getTime("time"));
                service.setWithHelper(resultSet.getBoolean("with_helper"));
                service.setServiceStatusId(resultSet.getLong("service_status_id"));

                services.add(service);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return services;
    }

    @Override
    public void add(Long recipientId, String fromPlace, String toPlace, Date date, Time time, boolean withHelper) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_SERVICE);
            preparedStatement.setLong(1, recipientId);
            preparedStatement.setString(2, fromPlace);
            preparedStatement.setString(3, toPlace);
            preparedStatement.setDate(4, date);
            preparedStatement.setTime(5, time);
            preparedStatement.setBoolean(6, withHelper);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void update(Long id, Long recipientId, String fromPlace, String toPlace, Date date, Time time, boolean withHelper) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_SERVICE_BY_ID);
            preparedStatement.setLong(1, recipientId);
            preparedStatement.setString(2, fromPlace);
            preparedStatement.setString(3, toPlace);
            preparedStatement.setDate(4, date);
            preparedStatement.setTime(5, time);
            preparedStatement.setBoolean(6, withHelper);
            preparedStatement.setLong(7, id);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void delete(Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(DELETE_SERVICE_BY_ID);
            preparedStatement.setLong(1, id);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void updateDriverAndStatus(String driverId, Long serviceStatusId, Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(UPDATE_SERVICE_DRIVER_STATUS_BY_ID);
            preparedStatement.setString(1, driverId);
            preparedStatement.setLong(2, serviceStatusId);
            preparedStatement.setLong(3, id);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void updateStatus(Long serviceStatusId, Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = connection.prepareStatement(UPDATE_SERVICE_STATUS_BY_ID);
            preparedStatement.setLong(1, serviceStatusId);
            preparedStatement.setLong(2, id);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }
}

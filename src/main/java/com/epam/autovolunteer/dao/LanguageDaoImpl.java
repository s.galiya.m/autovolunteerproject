package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.LanguageDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.Language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LanguageDaoImpl implements LanguageDao {
    private final static String GET_ALL_LANGUAGE = "SELECT * FROM language";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<Language> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<Language> languages = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_LANGUAGE);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Language language = new Language();
                language.setId(resultSet.getLong("id"));
                language.setCode(resultSet.getString("code").toUpperCase());
                language.setName(resultSet.getString("name"));
                languages.add(language);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return languages;
    }
}

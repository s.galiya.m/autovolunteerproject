package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.AutoDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.Auto;

import java.sql.*;

public class AutoDaoImpl implements AutoDao {
    private final static String GET_AUTO_BY_ID= "SELECT * FROM auto WHERE id = ?";
    private final static String GET_AUTO_BY_DRIVER_ID= "SELECT * FROM auto_extended WHERE driver_id = ?";
    private final static String ADD_AUTO = "INSERT INTO auto (id, auto_type_id, auto_model_id, color, description) " +
            "VALUES (?, ?, ?, ?, ?)";
    private final static String UPDATE_AUTO_BY_ID = "UPDATE auto SET auto_type_id = ?, auto_model_id = ?, color = ?, description = ? where id = ?";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public Auto getById(String id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        Auto auto = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_AUTO_BY_ID);
            preparedStatement.setString(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                auto = new Auto();
                auto.setId(resultSet.getString("id"));
                auto.setAutoTypeId(resultSet.getLong("auto_type_id"));
                auto.setAutoModelId(resultSet.getLong("auto_model_id"));
                auto.setColor(resultSet.getString("color"));
                auto.setDescription(resultSet.getString("description"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return auto;
    }

    @Override
    public Auto getByDriverId(String driver_id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        Auto auto = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_AUTO_BY_DRIVER_ID);
            preparedStatement.setString(1, driver_id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                auto = new Auto();
                auto.setId(resultSet.getString("id"));
                auto.setAutoTypeId(resultSet.getLong("auto_type_id"));
                auto.setAutoModelId(resultSet.getLong("auto_model_id"));
                auto.setColor(resultSet.getString("color"));
                auto.setDescription(resultSet.getString("description"));
                auto.setAutoType(resultSet.getString("type_name"));
                auto.setAutoModel(resultSet.getString("model_name"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return auto;
    }

    @Override
    public void add(String autoId, Long autoType, Long autoModel, String autoColor, String autoDesc) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_AUTO);
            preparedStatement.setString(1, autoId);
            preparedStatement.setLong(2, autoType);
            preparedStatement.setLong(3, autoModel);
            if (autoColor != null) {
                preparedStatement.setString(4, autoColor);
            } else {
                preparedStatement.setNull(4, Types.NULL);
            }
            if (autoDesc != null) {
                preparedStatement.setString(5,autoDesc);
            } else {
                preparedStatement.setNull(5, Types.NULL);
            }

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void update(Long autoType, Long autoModel, String autoColor, String autoDesc, String autoId) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_AUTO_BY_ID);

            preparedStatement.setLong(1, autoType);
            preparedStatement.setLong(2, autoModel);

            if (autoColor != null) {
                preparedStatement.setString(3, autoColor);
            } else {
                preparedStatement.setNull(3, Types.NULL);
            }
            if (autoDesc != null) {
                preparedStatement.setString(4,autoDesc);
            } else {
                preparedStatement.setNull(4, Types.NULL);
            }

            preparedStatement.setString(5, autoId);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }
}

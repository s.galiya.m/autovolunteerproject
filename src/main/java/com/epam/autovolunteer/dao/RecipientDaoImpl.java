package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.RecipientDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.Recipient;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RecipientDaoImpl implements RecipientDao {
    private final static String GET_ALL_RECIPIENT = "SELECT * FROM recipient";
    private final static String GET_RECIPIENT_BY_ID= "SELECT * FROM recipient WHERE id = ?";
    private final static String GET_RECIPIENT_BY_USER_ID= "SELECT * FROM recipient WHERE user_id = ?";
    private final static String ADD_RECIPIENT = "INSERT INTO recipient (user_id, disability_type_id, disability_group_id, address) VALUES (?, ?, ?, ?)";
    private final static String UPDATE_RECIPIENT_BY_ID = "UPDATE recipient SET disability_type_id = ?, disability_group_id = ?, address = ? where id = ?";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<Recipient> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<Recipient> recipients = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_RECIPIENT);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Recipient recipient = new Recipient();
                recipient.setId(resultSet.getLong("id"));
                recipient.setUserId(resultSet.getLong("user_id"));
                recipient.setDisabilityTypeId(resultSet.getLong("disability_type_id"));
                recipient.setDisabilityGroupId(resultSet.getLong("disability_group_id"));
                recipient.setAddress(resultSet.getString("address"));

                recipients.add(recipient);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return recipients;

    }

    @Override
    public Recipient getById(Long id) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        Recipient recipient = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_RECIPIENT_BY_ID);

            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                recipient = new Recipient();
                recipient.setId(resultSet.getLong("id"));
                recipient.setUserId(resultSet.getLong("user_id"));
                recipient.setDisabilityTypeId(resultSet.getLong("disability_type_id"));
                recipient.setDisabilityGroupId(resultSet.getLong("disability_group_id"));
                recipient.setAddress(resultSet.getString("address"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return recipient;
    }

    @Override
    public Recipient getByUserId(Long userId) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        Recipient recipient = null;

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_RECIPIENT_BY_USER_ID);

            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                recipient = new Recipient();
                recipient.setId(resultSet.getLong("id"));
                recipient.setUserId(resultSet.getLong("user_id"));
                recipient.setDisabilityTypeId(resultSet.getLong("disability_type_id"));
                recipient.setDisabilityGroupId(resultSet.getLong("disability_group_id"));
                recipient.setAddress(resultSet.getString("address"));
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return recipient;
    }

    @Override
    public void add(Long userId, Long disabilityTypeId, Long disabilityGroupId, String address) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_RECIPIENT);
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, disabilityTypeId);

            if (disabilityGroupId != null) {
                preparedStatement.setLong(3, disabilityGroupId);
            } else {
                preparedStatement.setNull(3, Types.NULL);
            }
            if (address != null) {
                preparedStatement.setString(4, address);
            } else {
                preparedStatement.setNull(4, Types.NULL);
            }
            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    @Override
    public void update(Long disType, Long disGroup, String address, Long recipientId) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_RECIPIENT_BY_ID);
            preparedStatement.setLong(1, disType);

            if (disGroup != null ) {
                preparedStatement.setLong(2, disGroup);
            } else {
                preparedStatement.setNull(2, Types.NULL);
            }
            if (address != null) {
                preparedStatement.setString(3, address);
            } else {
                preparedStatement.setNull(3, Types.NULL);
            }

            preparedStatement.setLong(4, recipientId);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }
}

package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.AutoModelDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.AutoModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AutoModelDaoImpl implements AutoModelDao {
    private final static String GET_ALL_AUTO_MODEL = "SELECT * FROM auto_model ORDER BY id";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<AutoModel> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<AutoModel> autoModels = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_AUTO_MODEL);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                AutoModel autoModel = new AutoModel();
                autoModel.setId(resultSet.getLong("id"));
                autoModel.setName(resultSet.getString("name"));
                autoModels.add(autoModel);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return autoModels;
    }
}

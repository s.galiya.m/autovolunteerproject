package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.DisabilityTypeDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;
import com.epam.autovolunteer.entity.DisabilityType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DisabilityTypeDaoImpl implements DisabilityTypeDao {
    private final static String GET_ALL_DISABILITY_TYPE = "SELECT * FROM disability_type ORDER BY id";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public List<DisabilityType> getAll() throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();
        List<DisabilityType> disabilityTypes = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(GET_ALL_DISABILITY_TYPE);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                DisabilityType disabilityType = new DisabilityType();
                disabilityType.setId(resultSet.getLong("id"));
                disabilityType.setName(resultSet.getString("name"));
                disabilityTypes.add(disabilityType);
            }
        } finally {
            connectionPool.returnConnection(connection);
        }
        return disabilityTypes;
    }
}

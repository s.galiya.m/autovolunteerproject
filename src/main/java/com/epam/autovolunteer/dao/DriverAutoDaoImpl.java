package com.epam.autovolunteer.dao;

import com.epam.autovolunteer.dao.interfaces.DriverAutoDao;
import com.epam.autovolunteer.database.connection.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DriverAutoDaoImpl implements DriverAutoDao {
    private final static String ADD_DRIVER_AUTO = "INSERT INTO driver_auto (driver_id, auto_id) VALUES (?, ?)";

    private ConnectionPool connectionPool;
    private Connection connection;

    @Override
    public void add(String driverId, String autoId) throws SQLException {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.takeConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(ADD_DRIVER_AUTO);
            preparedStatement.setString(1, driverId);
            preparedStatement.setString(2, autoId);

            preparedStatement.execute();

        } finally {
            connectionPool.returnConnection(connection);
        }
    }
}

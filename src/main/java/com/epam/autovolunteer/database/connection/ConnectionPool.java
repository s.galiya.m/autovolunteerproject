package com.epam.autovolunteer.database.connection;


import com.epam.autovolunteer.controller.VolunteerController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public final class ConnectionPool {
    private String driverName;
    private String url;
    private String user;
    private String password;
    private ResourceBundle bundle = ResourceBundle.getBundle("db");
    private final int POOL_SIZE = Integer.parseInt(bundle.getString("db.poolsize"));
    private static volatile ConnectionPool instance;
    private final BlockingQueue<Connection> connectionQueue = new ArrayBlockingQueue<>(POOL_SIZE);
    private static final Logger logger = LogManager.getLogger(ConnectionPool.class);


    private ConnectionPool() {
        setConnectionData();
        addConnection();
    }

    private void setConnectionData() {
        this.driverName = bundle.getString("db.driver");
        this.url = bundle.getString("db.url");
        this.user = bundle.getString("db.user");
        this.password = bundle.getString("db.password");
    }


    private void addConnection() {
        Connection connection;
        while (connectionQueue.size() < POOL_SIZE) {
            try {
//                DriverManager.registerDriver(new com.mysql.cj.jdbc.NonRegisteringDriver());
                Class.forName(driverName);
                connection = DriverManager.getConnection(url, user, password);
                connectionQueue.put(connection);
            } catch (InterruptedException | SQLException e) {
                logger.error(e);
            } catch (ClassNotFoundException e) {
                logger.error(e);
            }
        }
    }

    public static ConnectionPool getInstance() {
        ConnectionPool connectionPool = instance;
        if (connectionPool == null) {
            synchronized (ConnectionPool.class) {
                connectionPool = instance;
                if (connectionPool == null) {
                    instance = connectionPool = new ConnectionPool();
                }
            }
        }
        return connectionPool;
    }

    public synchronized Connection takeConnection() {
        Connection connection = null;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException e) {
            logger.error(e);
        }
        return connection;
    }

    public synchronized void returnConnection(Connection connection) {
        if ((connection != null) && (connectionQueue.size() <= POOL_SIZE)) {
            try {
                connectionQueue.put(connection);
            } catch (InterruptedException e) {
                logger.error(e);
            }
        }
    }
}

package com.epam.autovolunteer.filter;

import com.epam.autovolunteer.dao.LanguageDaoImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class LanguageFilter implements Filter {
    private static final Logger logger = LogManager.getLogger(LanguageFilter.class);
    private static final String LOCALE = "locale";

    private String defaultLocale;

    @Override
    public void init(FilterConfig filterConfig) {
        defaultLocale = filterConfig.getInitParameter("defaultLanguage");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession(true);

        LanguageDaoImpl languageDao = new LanguageDaoImpl();
        ServletContext servletContext = request.getServletContext();

        if (servletContext.getAttribute("languages") == null) {
            try {
                servletContext.setAttribute("languages", languageDao.getAll());
                servletContext.setAttribute("default_locale", defaultLocale.toUpperCase());
            } catch (SQLException throwables) {
                logger.error(throwables);
            }
        }

        String requestLocale = request.getParameter("lang");

        if (requestLocale != null ) {
            session.setAttribute(LOCALE, request.getParameter("lang").toUpperCase());
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}

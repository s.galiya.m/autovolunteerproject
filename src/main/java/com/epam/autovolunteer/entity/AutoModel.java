package com.epam.autovolunteer.entity;

import java.util.Objects;

public class AutoModel extends Entity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AutoModel)) return false;
        AutoModel autoModel = (AutoModel) o;
        return getId() == autoModel.getId() &&
                getName().equals(autoModel.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public String toString() {
        return "AutoModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

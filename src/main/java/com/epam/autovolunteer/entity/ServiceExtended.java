package com.epam.autovolunteer.entity;

import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

public class ServiceExtended extends Entity {
    private String driverId;
    private long recipientId;
    private String fromPlace;
    private String toPlace;
    private Date date;
    private Time time;
    private boolean withHelper;
    private long serviceStatusId;

    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private long cityId;

    private long userId;
    private long disabilityTypeId;
    private long disabilityGroupId;

    private long driverUserId;
    private String driverFirstName;
    private String driverLastName;
    private String driverPhone;

    private String autoId;
    private String color;
    private String autoType;
    private String autoModel;
    
    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public boolean isWithHelper() {
        return withHelper;
    }

    public void setWithHelper(boolean withHelper) {
        this.withHelper = withHelper;
    }

    public long getServiceStatusId() {
        return serviceStatusId;
    }

    public void setServiceStatusId(long serviceStatusId) {
        this.serviceStatusId = serviceStatusId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long user_id) {
        this.userId = user_id;
    }

    public long getDisabilityTypeId() {
        return disabilityTypeId;
    }

    public void setDisabilityTypeId(long disabilityTypeId) {
        this.disabilityTypeId = disabilityTypeId;
    }

    public long getDisabilityGroupId() {
        return disabilityGroupId;
    }

    public void setDisabilityGroupId(long disabilityGroupId) {
        this.disabilityGroupId = disabilityGroupId;
    }

    public long getDriverUserId() {
        return driverUserId;
    }

    public void setDriverUserId(long driverUserId) {
        this.driverUserId = driverUserId;
    }

    public String getDriverFirstName() {
        return driverFirstName;
    }

    public void setDriverFirstName(String driverFirstName) {
        this.driverFirstName = driverFirstName;
    }

    public String getDriverLastName() {
        return driverLastName;
    }

    public void setDriverLastName(String driverLastName) {
        this.driverLastName = driverLastName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getAutoId() {
        return autoId;
    }

    public void setAutoId(String autoId) {
        this.autoId = autoId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAutoType() {
        return autoType;
    }

    public void setAutoType(String autoType) {
        this.autoType = autoType;
    }

    public String getAutoModel() {
        return autoModel;
    }

    public void setAutoModel(String autoModel) {
        this.autoModel = autoModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServiceExtended)) return false;
        ServiceExtended that = (ServiceExtended) o;
        return getId() == that.getId() &&
                getRecipientId() == that.getRecipientId() &&
                isWithHelper() == that.isWithHelper() &&
                getServiceStatusId() == that.getServiceStatusId() &&
                getCityId() == that.getCityId() &&
                getUserId() == that.getUserId() &&
                getDisabilityTypeId() == that.getDisabilityTypeId() &&
                getDisabilityGroupId() == that.getDisabilityGroupId() &&
                getDriverUserId() == that.getDriverUserId() &&
                getDriverId().equals(that.getDriverId()) &&
                getFromPlace().equals(that.getFromPlace()) &&
                getToPlace().equals(that.getToPlace()) &&
                getDate().equals(that.getDate()) &&
                getTime().equals(that.getTime()) &&
                getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getPhone().equals(that.getPhone()) &&
                getEmail().equals(that.getEmail()) &&
                Objects.equals(getDriverFirstName(), that.getDriverFirstName()) &&
                Objects.equals(getDriverLastName(), that.getDriverLastName()) &&
                Objects.equals(getDriverPhone(), that.getDriverPhone()) &&
                Objects.equals(getAutoId(), that.getAutoId()) &&
                Objects.equals(getColor(), that.getColor()) &&
                Objects.equals(getAutoType(), that.getAutoType()) &&
                Objects.equals(getAutoModel(), that.getAutoModel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDriverId(), getRecipientId(), getFromPlace(), getToPlace(), getDate(), getTime(), isWithHelper(), getServiceStatusId(), getFirstName(), getLastName(), getPhone(), getEmail(), getCityId(), getUserId(), getDisabilityTypeId(), getDisabilityGroupId(), getDriverUserId(), getDriverFirstName(), getDriverLastName(), getDriverPhone(), getAutoId(), getColor(), getAutoType(), getAutoModel());
    }

    @Override
    public String toString() {
        return "ServiceExtended{" +
                "id=" + id +
                ", driverId='" + driverId + '\'' +
                ", recipientId=" + recipientId +
                ", fromPlace='" + fromPlace + '\'' +
                ", toPlace='" + toPlace + '\'' +
                ", date=" + date +
                ", time=" + time +
                ", withHelper=" + withHelper +
                ", serviceStatusId=" + serviceStatusId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", cityId=" + cityId +
                ", userId=" + userId +
                ", disabilityTypeId=" + disabilityTypeId +
                ", disabilityGroupId=" + disabilityGroupId +
                ", driverUserId=" + driverUserId +
                ", driverFirstName='" + driverFirstName + '\'' +
                ", driverLastName='" + driverLastName + '\'' +
                ", driverPhone='" + driverPhone + '\'' +
                ", autoId='" + autoId + '\'' +
                ", color='" + color + '\'' +
                ", autoType='" + autoType + '\'' +
                ", autoModel='" + autoModel + '\'' +
                '}';
    }
}

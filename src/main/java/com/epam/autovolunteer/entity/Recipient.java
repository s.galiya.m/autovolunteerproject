package com.epam.autovolunteer.entity;

import java.util.Objects;

public class Recipient extends Entity {

    protected long userId;
    protected long disabilityTypeId;
    protected long disabilityGroupId;
    protected String address;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getDisabilityTypeId() {
        return disabilityTypeId;
    }

    public void setDisabilityTypeId(long disabilityTypeId) {
        this.disabilityTypeId = disabilityTypeId;
    }

    public long getDisabilityGroupId() {
        return disabilityGroupId;
    }

    public void setDisabilityGroupId(long disabilityGroupId) {
        this.disabilityGroupId = disabilityGroupId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Recipient)) return false;
        Recipient recipient = (Recipient) o;
        return getId() == recipient.getId() &&
                getUserId() == recipient.getUserId() &&
                getDisabilityTypeId() == recipient.getDisabilityTypeId() &&
                getDisabilityGroupId() == recipient.getDisabilityGroupId() &&
                getAddress().equals(recipient.getAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUserId(), getDisabilityTypeId(), getDisabilityGroupId(), getAddress());
    }

    @Override
    public String toString() {
        return "Recipient{" +
                "id=" + id +
                ", userId=" + userId +
                ", disabilityTypeId=" + disabilityTypeId +
                ", disabilityGroupId=" + disabilityGroupId +
                ", address='" + address + '\'' +
                '}';
    }
}

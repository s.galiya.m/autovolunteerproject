package com.epam.autovolunteer.entity;

import java.util.Objects;

public class User extends Entity {
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String password;
    private long cityId;
    private boolean isAdmin;
    private boolean isDriver;
    private boolean isRecipient;
    private boolean isBlocked;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isDriver() {
        return isDriver;
    }

    public void setDriver(boolean driver) {
        isDriver = driver;
    }

    public boolean isRecipient() {
        return isRecipient;
    }

    public void setRecipient(boolean recipient) {
        isRecipient = recipient;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId() == user.getId() &&
                getCityId() == user.getCityId() &&
                isAdmin() == user.isAdmin() &&
                isDriver() == user.isDriver() &&
                isRecipient() == user.isRecipient() &&
                isBlocked() == user.isBlocked() &&
                getFirstName().equals(user.getFirstName()) &&
                getLastName().equals(user.getLastName()) &&
                getPhone().equals(user.getPhone()) &&
                getEmail().equals(user.getEmail()) &&
                getPassword().equals(user.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getPhone(), getEmail(), getPassword(), getCityId(), isAdmin(), isDriver(), isRecipient(), isBlocked());
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", cityId=" + cityId +
                ", isAdmin=" + isAdmin +
                ", isDriver=" + isDriver +
                ", isRecipient=" + isRecipient +
                ", isBlocked=" + isBlocked +
                '}';
    }
}

package com.epam.autovolunteer.entity;

import java.util.Objects;

public class RecipientExtended extends Recipient{
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private long cityId;
    private String photo;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RecipientExtended)) return false;
        if (!super.equals(o)) return false;
        RecipientExtended that = (RecipientExtended) o;
        return getCityId() == that.getCityId() &&
                getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getPhone().equals(that.getPhone()) &&
                getEmail().equals(that.getEmail()) &&
                Objects.equals(getPhoto(), that.getPhoto());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFirstName(), getLastName(), getPhone(), getEmail(), getCityId(), getPhoto());
    }

    @Override
    public String toString() {
        return "RecipientExtended{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", cityId=" + cityId +
                ", photo='" + photo + '\'' +
                ", id=" + id +
                ", userId=" + userId +
                ", disabilityTypeId=" + disabilityTypeId +
                ", disabilityGroupId=" + disabilityGroupId +
                ", address='" + address + '\'' +
                '}';
    }
}

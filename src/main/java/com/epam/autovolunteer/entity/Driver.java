package com.epam.autovolunteer.entity;

import java.util.Objects;

public class Driver {
    private String id;
    private long userId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Driver)) return false;
        Driver driver = (Driver) o;
        return getUserId() == driver.getUserId() &&
                getId().equals(driver.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUserId());
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id='" + id + '\'' +
                ", userId=" + userId +
                '}';
    }
}

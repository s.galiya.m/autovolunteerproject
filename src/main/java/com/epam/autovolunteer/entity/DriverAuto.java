package com.epam.autovolunteer.entity;

import java.util.Objects;

public class DriverAuto {
    private String driverId;
    private String autoId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getAutoId() {
        return autoId;
    }

    public void setAutoId(String autoId) {
        this.autoId = autoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DriverAuto)) return false;
        DriverAuto that = (DriverAuto) o;
        return getDriverId().equals(that.getDriverId()) &&
                getAutoId().equals(that.getAutoId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDriverId(), getAutoId());
    }

    @Override
    public String toString() {
        return "DriverAuto{" +
                "driverId='" + driverId + '\'' +
                ", autoId='" + autoId + '\'' +
                '}';
    }
}

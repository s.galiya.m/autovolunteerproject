package com.epam.autovolunteer.entity;

import java.util.Objects;

public class AutoType extends Entity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AutoType)) return false;
        AutoType autoType = (AutoType) o;
        return getId() == autoType.getId() &&
                getName().equals(autoType.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public String toString() {
        return "AutoType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

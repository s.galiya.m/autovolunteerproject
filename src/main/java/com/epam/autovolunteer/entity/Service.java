package com.epam.autovolunteer.entity;

import java.sql.Time;
import java.sql.Date;
import java.util.Objects;

public class Service extends Entity {

    private String driverId;
    private long recipientId;
    private String fromPlace;
    private String toPlace;
    private Date date;
    private Time time;
    private boolean withHelper;
    private long serviceStatusId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public boolean isWithHelper() {
        return withHelper;
    }

    public void setWithHelper(boolean withHelper) {
        this.withHelper = withHelper;
    }

    public long getServiceStatusId() {
        return serviceStatusId;
    }

    public void setServiceStatusId(long serviceStatusId) {
        this.serviceStatusId = serviceStatusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Service)) return false;
        Service service = (Service) o;
        return getId() == service.getId() &&
                getRecipientId() == service.getRecipientId() &&
                isWithHelper() == service.isWithHelper() &&
                getServiceStatusId() == service.getServiceStatusId() &&
                getDriverId().equals(service.getDriverId()) &&
                getFromPlace().equals(service.getFromPlace()) &&
                getToPlace().equals(service.getToPlace()) &&
                getDate().equals(service.getDate()) &&
                getTime().equals(service.getTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDriverId(), getRecipientId(), getFromPlace(), getToPlace(), getDate(), getTime(), isWithHelper(), getServiceStatusId());
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", driverId='" + driverId + '\'' +
                ", recipientId=" + recipientId +
                ", fromPlace='" + fromPlace + '\'' +
                ", toPlace='" + toPlace + '\'' +
                ", date=" + date +
                ", time=" + time +
                ", withHelper=" + withHelper +
                ", serviceStatusId=" + serviceStatusId +
                '}';
    }
}

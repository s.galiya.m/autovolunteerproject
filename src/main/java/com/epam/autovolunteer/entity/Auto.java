package com.epam.autovolunteer.entity;

import java.util.Objects;

public class Auto {
    private String id;
    private long autoTypeId;
    private long autoModelId;
    private String color;
    private String description;
    private String autoType;
    private String autoModel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getAutoTypeId() {
        return autoTypeId;
    }

    public void setAutoTypeId(long autoTypeId) {
        this.autoTypeId = autoTypeId;
    }

    public long getAutoModelId() {
        return autoModelId;
    }

    public void setAutoModelId(long autoModelId) {
        this.autoModelId = autoModelId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAutoType() {
        return autoType;
    }

    public void setAutoType(String autoType) {
        this.autoType = autoType;
    }

    public String getAutoModel() {
        return autoModel;
    }

    public void setAutoModel(String autoModel) {
        this.autoModel = autoModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Auto)) return false;
        Auto auto = (Auto) o;
        return getAutoTypeId() == auto.getAutoTypeId() &&
                getAutoModelId() == auto.getAutoModelId() &&
                getId().equals(auto.getId()) &&
                Objects.equals(getColor(), auto.getColor()) &&
                Objects.equals(getDescription(), auto.getDescription()) &&
                getAutoType().equals(auto.getAutoType()) &&
                getAutoModel().equals(auto.getAutoModel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAutoTypeId(), getAutoModelId(), getColor(), getDescription(), getAutoType(), getAutoModel());
    }

    @Override
    public String toString() {
        return "Auto{" +
                "id='" + id + '\'' +
                ", autoTypeId=" + autoTypeId +
                ", autoModelId=" + autoModelId +
                ", color='" + color + '\'' +
                ", description='" + description + '\'' +
                ", autoType='" + autoType + '\'' +
                ", autoModel='" + autoModel + '\'' +
                '}';
    }
}

package com.epam.autovolunteer.entity;

import java.util.Objects;

public class DisabilityType extends Entity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DisabilityType)) return false;
        DisabilityType that = (DisabilityType) o;
        return getId() == that.getId() &&
                getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public String toString() {
        return "DisabilityType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

package com.epam.autovolunteer.entity;

import java.util.Objects;

public class ServiceStatus extends Entity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServiceStatus)) return false;
        ServiceStatus orderType = (ServiceStatus) o;
        return getId() == orderType.getId() &&
                getName().equals(orderType.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public String toString() {
        return "OrderType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
